from matplotlib.pyplot import show, savefig, colorbar
import matplotlib.pyplot as plt




from numpy import array, log10, isnan, zeros, str_, argsort, triu, nonzero


def draw_heatmap(data,disease,stat):
    """ draw heatmaps"""
    LD_array=data.assoc['LD_mat'].toarray()  # convert to array
    #plt.figure()
    #plt.subplots_adjust(top=0.3)        
    plt.matshow(LD_array)
    tmp_chroff=zeros(data.num_vertex,dtype=(str_,16))
    for idx, elem in enumerate(data.snp):
        tmp_chroff[idx] = 'chr%02s-%d' % (elem['chrom'], elem['bp_position']) 

    plt.xticks(range(data.num_vertex)[::100],tmp_chroff[::100],rotation='vertical',fontsize=5)
    plt.yticks(range(data.num_vertex)[::100],tmp_chroff[::100],fontsize=5)
    colorbar()
    #plt.title('%s %s GWIS pairs r2 heatmap' %(disease,stat),verticalalignment='top')
    savefig('%s_LD_mat_%s.pdf' % (disease,stat), dpi=300, orientation='landscape')
    ## show -log(LD_mat), so low LD values are displayed
    plt.clf()

    LD_aLog=-log10(LD_array)
    plt.matshow(LD_aLog)
    tmp_chroff=zeros(data.num_vertex,dtype=(str_,16))
    for idx, elem in enumerate(data.snp):
        tmp_chroff[idx] = 'chr%02s-%d' % (elem['chrom'], elem['bp_position']) 

    
    plt.xticks(range(data.num_vertex)[::100],tmp_chroff[::100],rotation='vertical',fontsize=5)
    plt.yticks(range(data.num_vertex)[::100],tmp_chroff[::100],fontsize=5)
    #plt.spines["top"].set_visible(False)
    #plt.get_xaxis().tick_bottom()

    plt.colorbar()
    #plt.title('%s %s GWIS pairs -log10(r2) heatmap' %(disease,stat))
    savefig('%s_LD_mat_neg_log_%s.pdf' % (disease,stat), dpi=300, orientation='landscape')
    plt.clf()

def plot_hist(data,disease,stat):
    """ plot histogram showing frequency of GWIS pairs with r2 values """
    LD_array=data.assoc['LD_mat'].toarray()  # convert to array
    LD_array_upper = triu(LD_array)
    #vals=-log10(LD_array[nonzero(LD_array)])  ## assume values of 0.0 are not gwis pairs
    vals_upper=LD_array_upper[nonzero(LD_array_upper)]     
    plt.hist(vals_upper.tolist(),range=[0,1.0])
    plt.xlabel('r2 GWIS pairs')
    plt.ylabel('frequency')
    plt.title('%s %s GWIS pairs r2 frequency histogram' % (disease,stat))
    #plt.show()
    savefig('%s_LD_hist_%s.pdf' % (disease,stat), dpi=300, orientation='landscape')
    plt.clf()



# plot looks strange
def plot_bp_vs_ld(data,disease,stat):
    """ plot LD vs bp distance. """    
    LD_array=data.assoc['LD_mat'].toarray()  # convert to array
    #LD_aLog=-log10(LD_array)
    #LD_array_upper = numpy.triu(LD_array)
    #vals=-log10(LD_array[nonzero(LD_array)])  ## assume values of 0.0 are not gwis pairs
    #vals_upper=LD_array_upper[nonzero(LD_array_upper)]
    
    bp=data.bp_distance() # pre_compute bp distance
    #bp_upper=numpy.triu(bp)
    #bp_vals_upper=bp_upper
    
    x=[] # empty list
    y=[]
    for i in range(bp.shape[0]):
        for j in range(bp.shape[1]):
            if i<j: # only take upper values, dont need diagonal
                if(bp[i][j] !=0 and not isnan(bp[i][j]) and bp[i][j] !=-1): # not on diagonal,in gwis,not on diff chromosome
                    x.append(bp[i][j])
                    y.append(LD_array[i][j])

    x_sort=[]
    y_sort=[]
    for i,j in enumerate(argsort(x)):
        y_sort.append(y[j])

    x_sort=x  # this acts like a pointer, so LOSE x
    x_sort.sort()

    #plt.plot(x_sort,y_sort,linestyle='None') # have to fix linestyle to get point, not continuous line
    plt.plot(x_sort,y_sort,'ro') # have to fix linestyle to get point, not continuous line
    #plt.yrange(0,max(y_sort))
    plt.axis([0,100000,0,1])
    plt.xlabel('bp distance')
    plt.ylabel('r2')
    plt.title('r2 vs bp distance')
    plt.show    
    savefig('%s_LDvbpdistance_%s.pdf' % (disease,stat), dpi=300, orientation='landscape')
    plt.clf()

# function needs work, plot r2 not log. Also, take upper trianle so values arent repeated
# plot looks strange
def plot_bp_vs_gwis(data,disease,stat):
    """ plot gwis score vs bp distance. """    
    gwis_array=data.adj_mat.toarray()  # convert to array
    #LD_aLog=-log10(LD_array)
    #LD_array_upper = numpy.triu(LD_array)
    #vals=-log10(LD_array[nonzero(LD_array)])  ## assume values of 0.0 are not gwis pairs
    #vals_upper=LD_array_upper[nonzero(LD_array_upper)]
    
    bp=data.bp_distance() # pre_compute bp distance
    #bp_upper=numpy.triu(bp)
    #bp_vals_upper=bp_upper
    
    x=[] # empty list
    y=[]
    for i in range(bp.shape[0]):
        for j in range(bp.shape[1]):
            if i<j: # only take upper values, dont need diagonal
                if(bp[i][j] !=0 and not isnan(bp[i][j]) and bp[i][j] !=-1): # not on diagonal,in gwis,not on diff chromosome
                    x.append(bp[i][j])
                    y.append(gwis_array[i][j])

    x_sort=[]
    y_sort=[]
    for i,j in enumerate(argsort(x)):
        y_sort.append(y[j])

    x_sort=x  # this acts like a pointer, so LOSE x
    x_sort.sort()

    #plt.plot(x_sort,y_sort,linestyle='None') # have to fix linestyle to get point, not continuous line
    plt.plot(x_sort,y_sort,'ro') # have to fix linestyle to get point, not continuous line
    #plt.yrange(0,max(y_sort))
    plt.axis([0,100000,0,50])
    plt.xlabel('bp distance')
    plt.ylabel('GWIS score')
    plt.title('GWIS vs bp distance')
    plt.show    
    savefig('%s_gwisvbpdistance_%s.pdf' % (disease,stat), dpi=300, orientation='landscape')
    plt.clf()

