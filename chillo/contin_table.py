import os, subprocess
from chillo.io_plink import GenotypeData
from numpy import array, flatnonzero
import numpy


"""Code to compute contingency tables from genotype data."""

def contingency_table(idx_snp1, idx_snp2, idx_controls, idx_cases, test_face):
    """Return three contingency tables corresponding to the two genotypes
    for a case control study phenotype.
    - 3 by 1 tables for geno1 and geno2 respectively
    - 3 by 3 tables for the bivariate table
    """

    #test_face = GenotypeData(filename)
    #test_face.open_file()
    geno1_vec = test_face.get_genotype(idx_snp1)
    geno2_vec = test_face.get_genotype(idx_snp2)
    
    #assert((idx_cases == test_face.get_idx_case()).all())
    #assert((idx_controls == test_face.get_idx_control()).all())

    #contin1 = contingency_table_univariate(idx_controls, idx_cases, geno1_vec)
    #contin2 = contingency_table_univariate(idx_controls, idx_cases, geno2_vec)
    bivar = contingency_table_bivariate(idx_controls, idx_cases, geno1_vec, geno2_vec)

    #return (table_univariate1, table_univariate2,bivar, [total control, total cases]  ) 

    #test_face.close_file()



    return numpy.sum(bivar,2),numpy.sum(bivar,1), bivar, numpy.sum(numpy.sum(bivar,2),1) 



def contingency_table_univariate(idx_controls, idx_cases, geno_vec):
    """
    return a table univariate for case and controls from one genotype
    """
    # [control,cases]
 
    ret_data=numpy.zeros((3,2), dtype=int)
 
    for genotype in [0,1,2]:
        i=0
        for group in [idx_controls, idx_cases]:  
            count = len(flatnonzero(geno_vec[group] == genotype))
            ret_data[genotype,i]=count
            i=i+1
        count = len(flatnonzero(geno_vec == genotype))
    return ret_data

def _contingency_table_bivariate(geno1_vec, geno2_vec):
    """
    return a 3x3 table for the two genotype vectors
    """
    con_table = numpy.zeros((3,3), dtype=int)
    for genotype1 in [0,1,2]:
        for genotype2 in [0,1,2]:
            con_table[genotype1, genotype2] = numpy.sum(numpy.logical_and(geno1_vec==genotype1,
                                                                          geno2_vec==genotype2))
    return con_table

def contingency_table_bivariate(idx_controls, idx_cases, geno1_vec, geno2_vec):
    """
    return a table bivariate for case and controls from two genotype
    """  
    ret_data = numpy.zeros((2,3,3), dtype=int)
    v1 = geno1_vec[idx_controls]
    v2 = geno2_vec[idx_controls]
    ret_data[0,:,:] = _contingency_table_bivariate(v1, v2)
    v1 = geno1_vec[idx_cases]
    v2 = geno2_vec[idx_cases]
    ret_data[1,:,:] = _contingency_table_bivariate(v1, v2)
    return ret_data

def contingency_table_bivariate_old(idx_controls, idx_cases, geno1_vec, geno2_vec):
    """
    return a table bivariate for case and controls from two genotype
    """  
    ret_data=numpy.zeros((2,3,3), dtype=int)
    for genotype1 in [0,1,2]:            
        for genotype2 in [0,1,2]:
            i=0
            for group in [idx_controls,idx_cases]:
                count=0                                      
                for idx in group:  
                    if(geno1_vec[idx] == genotype1 and geno2_vec[idx] == genotype2 ):
                        count=count+1
                ret_data[i,genotype1,genotype2]=count 
                i=i+1    
    return ret_data






def test_ct():

    a = contingency_table(111, 500, '/home/cristovao/Desktop/imp_files/bdWTC')[2]

    print("\n\nteste 1 \n\n")
    print("\ntable_bivariate\n",a)
    print("\ntable_univariate1 from table_bivariate\n", str(numpy.sum(a,2)))
    print("\ntable_univariate2 from table_bivariate\n",numpy.sum(a,1))
    print("\ntest-total [control,cases]\n", numpy.sum(numpy.sum(a,2),1))
    print("\ntest-total [control,cases]\n", numpy.sum(numpy.sum(a,1),1))

    print("\n\nteste 2 \n\n")

    b = contingency_table(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC') 
    print("\ntable_bivariate\n",b[2])
    print("\ntable_univariate1 from table_bivariate\n", b[0])
    print("\ntable_univariate2 from table_bivariate\n", b[1])
    print("\ntest-total [control,cases]\n",b[3])

    print("\n\nteste 3 \n\n")

    b = contingency_table(13, 402, '/home/cristovao/Desktop/imp_files/bdWTC') 
    print("\ntable_bivariate\n",b[2])
    print("\ntable_univariate1 from table_bivariate\n", b[0])
    print("\ntable_univariate2 from table_bivariate\n", b[1])
    print("\ntest-total [control,cases]\n",b[3])


    #b = contingency_table(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC') 
    #num_rows, num_cols = b[2][0].shape
    #print "\ntable_univariate1 from table_bivariate\n",b[0][0],num_rows, num_cols
    #test_control_ok=0
    #test_cases_ok=0
    #for l in range(3):
    #    for c in range(3):
    #        print b[2][0][l][c]/float(b[3][0]),b[2][1][l][c]/float(b[3][1])
    #        test_control_ok=test_control_ok+b[2][0][l][c]/float(b[3][0])
    #        test_cases_ok=test_cases_ok+b[2][1][l][c]/float(b[3][1])
    #print "\n",test_control_ok,test_cases_ok,"\n"


    #dataset=contingency_table(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC')
    #str_json='{"unv1": [{"controls":'+str(dataset[0][0][0])+' ,"cases":'+str(dataset[0][1][0])+" }, "+'{"controls":'+str(dataset[0][0][1])+' ,"cases":'+str(dataset[0][1][1])+" }, "+'{"controls":'+str(dataset[0][0][2])+' ,"cases":'+str(dataset[0][1][2])+" }], " 
    #print str_json,"\n"
    #str_json=str_json+'"unv2": [{"controls":'+str(dataset[1][0][0])+' ,"cases":'+str(dataset[1][1][0])+" }, "+'{"controls":'+str(dataset[1][0][1])+' ,"cases":'+str(dataset[1][1][1])+" }, "+'{"controls":'+str(dataset[1][0][2])+' ,"cases":'+str(dataset[1][1][2])+" }], "
    #print str_json,"\n"
    #str_json=str_json+'"biv": ['
    #for l in range(3):
    #       line='['
    #       for c in range(3):
    #                line=line+'{"controls":'+str(dataset[2][0][l][c])+' ,"cases":'+str(dataset[2][1][l][c])+'}, '                     
    #       str_json=str_json+line[:-2]+'],'
    #print str_json[:-1]+"]}","\n"           
             

if __name__ == '__main__':
    test_ct()  

    





