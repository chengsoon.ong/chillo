import os, subprocess
import tables
from chillo.io_hdf5 import GenotypeData
from numpy import array, flatnonzero
import numpy
import itertools



"""Code to compute ROC plot from genotype data."""




def comb_case_control(n):
    """return a matrix with all possibles combinatio
        among 1 and -1 to create diferents tuples with
        3 or 9 elementes 
    """

    v=[1,-1]

    combos = itertools.product(v, repeat=n)
   
    return combos



def compute_phen_from_gen1(geno,comb_pheno):
    """ 
    Compute a phenotype from a genotype and a comb of the 3 values 
    with 2 possibles values e.g (1,-1,1) -> (case,control,case)
    """

    compute_pheno=[]
    for e in geno:
        compute_pheno.append(comb_pheno[e])

    return compute_pheno


def compute_phen_from_gen1andgen2(geno1,geno2,comb_pheno):    
    """ 
    Compute a phenotype from two genotypes and a comb of the 9 values with 2 possibles values e.g 
    (1,-1,1,-1,1,1,1,1,1) -> (case,control,case,control,case,case,case,case,case)
    """
    
    if(len(geno1)!=len(geno2)):
        print("ERROR: genotype1 and genotype2 have diferent length")
    
    dic={}
    dic[0,0]= comb_pheno[0]
    dic[1,0]= comb_pheno[1]
    dic[2,0]= comb_pheno[2]
    dic[0,1]= comb_pheno[3]
    dic[1,1]= comb_pheno[4]
    dic[2,1]= comb_pheno[5]
    dic[0,2]= comb_pheno[6]
    dic[1,2]= comb_pheno[7]
    dic[2,2]= comb_pheno[8]

#    idx=0
#    for j in (0,1,2):
#        for i in (0,1,2):
#            dic[i,j]=comb_pheno[idx]
#            idx=idx+1        

    compute_pheno=[]
    for i in range(len(geno1)):        
        compute_pheno.append(dic[geno1[i],geno2[i]])

    return compute_pheno



def compute_tp_fp_tn_fn(comp_pheno, idx_cases, idx_controls):

    """ 
    return the values TP,FP,TN and FN from a phenotype 
    computed and idx_cases and idx_controls 
    """
    v=[]
    comp_pheno=array(comp_pheno)
    for phenotype in [1,-1]:
            for group in [idx_cases, idx_controls]:  
                count = len(flatnonzero(comp_pheno[group] == phenotype))        
                v.append(count) #[tp,fp,fn,tp]
    return v #[tp,fp,fn,tp]


def compute_FPR_TPR(comp_pheno, idx_cases, idx_controls):
    """
    return the values FPR and TPR from a phenotype 
    computed and idx_cases and idx_controls     
    """
    v=compute_tp_fp_tn_fn(comp_pheno, idx_cases, idx_controls)    

    return  float(v[1])/float(v[1]+v[3]),float(v[0])/float(v[0]+v[2])


def roc_plot(idx_snp1, idx_snp2, filename):
    """Return three contingency tables corresponding to the two genotypes
    for a case control study phenotype.
    - 3 by 1 tables for geno1 and geno2 respectively
    - 3 by 3 tables for the bivariate table
    """
    contin1=[]
    contin2=[]
    bivar=[]

    test_face = GenotypeData(filename)

    test_face.open_file()

    geno1_vec = test_face.get_genotype(idx_snp1)
    geno2_vec = test_face.get_genotype(idx_snp2)
    idx_cases = test_face.get_idx_case()
    idx_controls = test_face.get_idx_control()

    # -------  tests  ---------
    #geno1_vec = [0 ,0 , 1, 2, 0, 2, 1, 0]
    #geno2_vec = [0 ,1 , 1, 2, 2, 0, 0, 2]
    #idx_cases=[0,1,5,7]
    #idx_controls=[2,3,4,6]



    for e in comb_case_control(3):
        #print compute_FPR_TPR(compute_phen_from_gen1(geno1_vec,e), idx_cases, idx_controls)
        #print compute_FPR_TPR(compute_phen_from_gen1(geno2_vec,e), idx_cases, idx_controls)            
        contin1.append(compute_FPR_TPR(compute_phen_from_gen1(geno1_vec,e), idx_cases, idx_controls))
        contin2.append(compute_FPR_TPR(compute_phen_from_gen1(geno2_vec,e), idx_cases, idx_controls))

    for e in comb_case_control(9):
        #print compute_FPR_TPR(compute_phen_from_gen1andgen2(geno1_vec,geno2_vec,e), idx_cases, idx_controls)
        bivar.append(compute_FPR_TPR(compute_phen_from_gen1andgen2(geno1_vec,geno2_vec,e), idx_cases, idx_controls))



    return contin1, contin2, bivar




def test_r():

    g1 = [0 ,0 , 1, 2, 0, 2, 1, 0]
    g2 = [0 ,1 , 1, 2, 2, 0, 0, 2]

    #p  = [1 ,1 ,-1,-1,-1, 1,-1, 1]

    idx_cases=[0,1,5,7]
    idx_controls=[2,3,4,6]

    #comp_pheno=[1,1,1,1,1,1,1,1]


    #print compute_FPR_TPR([1,1,1,1,1,1,1,1], idx_cases, idx_controls)
    #print compute_FPR_TPR([1,1,1,-1,1,-1,1,1], idx_cases, idx_controls)
    #print compute_FPR_TPR([-1,1,-1,-1,1,1,-1,1], idx_cases, idx_controls)



    #i=1
    #for e in comb_case_control(3):
            
    #         print "\n\n\n",i,e,compute_phenotype(geno1_vec,e)

        # print "comp_phen=",i,compute_phen_from_gen1(g1,e),"[tp,fp,fn,tp]",(compute_FPR_TPR(compute_phen_from_gen1(g1,e), idx_cases, idx_controls))
         #print "comp_phen=",i,compute_phen_from_gen1(g2,e),"[tp,fp,fn,tp]",(compute_FPR_TPR(compute_phen_from_gen1(g2,e), idx_cases, idx_controls)),"\n"
#         i=i+1

    #i=1
    #for e in comb_case_control(9):
    
         #print "comp_phen=",i,compute_phen_from_gen1andgen2(g1,g2,e),"[tp,fp,fn,tp]",(compute_FPR_TPR(compute_phen_from_gen1andgen2(g1,g2,e), idx_cases, idx_controls)),"\n"
#         i=i+1





    #print _get_json_roc(173422, 173424, '/home/cristovao/Desktop/imp_files/t1dWTC')
    print(_get_json_roc(173421, 173423, '/home/cristovao/Desktop/imp_files/t1dWTC'))

def _get_json_roc(idx1, idx2,filename):
        """  """

        dataset=roc_plot(idx1, idx2, filename)


        str_json='{"gen1":['
    
        for i in dataset[0]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '
        
        str_json=str_json[:-2]+'] ,"gen2":['

        for i in dataset[1]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '

        str_json=str_json[:-2]+'] ,"gen1gen2":['
        

        for i in dataset[2]:
            str_json=str_json+'{"FPR":'+str(i[0])+' ,"TPR":'+str(i[1])+'}, '

        return str_json[:-2]+']}'





    #print roc_plot(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC')[0]
    #print '\n\n\n',roc_plot(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC')[1]
    #print '\n\n\n',roc_plot(1, 42, '/home/cristovao/Desktop/imp_files/bdWTC')[2]




if __name__ == '__main__':
    test_r()  










