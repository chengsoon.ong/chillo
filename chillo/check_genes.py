"""Check whether any of the discovered SNPs are in genes"""

import pybedtools as bt
from chillo.named_array import NArray
from chillo.tools_config import configure_from_file
import csv

def parse_filename(filename):
    """Returns the disease and test statistic as strings"""
    cur_file = filename.split('/')[-1]
    disease = cur_file
    #(disease, rest) = cur_file.split('WTC') # EDIT TO split filename if necessary
    #(stat, rest) = rest.split('_Abs')
    stat = 'RoP'  # Edit stat 
    return disease, stat

# Edit - remove gene_names_outfile from argument list. Create separate function to write csv file with gene names
def find_annotation(snp_file, anno_file, ref_file, anno_type, near_bp=0, negative=False, gene_names_outfile='None'):
    """Use pybedtools to intersect the bed files"""
    anno = bt.BedTool(anno_file)
    snps = bt.BedTool(snp_file)
    ref = bt.BedTool(ref_file)
    #ref.inside=ref.intersect(anno,u=True)
    anno.inside=anno.intersect(ref,wb=True) # wb=True, keep columns of ref
    print('Number of SNPs: %d' % snps.count())
    if negative:
        #inside = snps.intersect(anno, v=True)
        inside = anno.inside.intersect(snps, v=True)
        print('%d SNPs are not in %ss' % (len(inside), anno_type))
    else:
        #inside = snps.intersect(anno, u=True)
        inside = anno.inside.intersect(snps, u=True)
        print('%d SNPs are in %ss' % (len(inside), anno_type))
    if len(inside) > 0:
        print(inside.head())
        if gene_names_outfile is not 'None':
            csvfile = open(gene_names_outfile, 'w')        
            csvfile.write('inside genes within window %d' % near_bp)
            csvfile.write('\n')
            for idx, item in enumerate(inside):
                csvfile.write(item[9] + '\n')
            csvfile.close()
        else:
            print("Not writing gene name output file")
    if near_bp > 0:
        if negative:
            nearby = snps.window(anno, w=near_bp, v=True)
            print('%d SNPs are not within %d bases of a %s' % (len(nearby), near_bp, anno_type))
        else:
            #nearby = snps.window(anno, w=near_bp, u=True)
            nearby = anno.inside.window(snps, w=near_bp, u=True)
            print('%d SNPs are within %d bases of a %s' % (len(nearby), near_bp, anno_type))
        if len(nearby) > 0:
            # write gene names to outfile
            print(nearby.head())
            if gene_names_outfile is not 'None':
                csvfile = open(gene_names_outfile, 'w')        
                csvfile.write('nearby genes within window %d' % near_bp)
                csvfile.write('\n')
                for idx, item in enumerate(nearby):
                    csvfile.write(item[9] + '\n')
                csvfile.close()
            else:
                print("Not writing gene name output file")
        return snps.count(), inside.count(), nearby.count()
    return snps.count(), inside.count()

def classify_loci(filename,config, near_bp=10000,gene_names_outfile='None'):
    """Check the location of the SNPs with genome annotations"""
    print('Using bedtools')
    ref_file=config.get('ANNO_REFFLAT')
    snps, coding = find_annotation(filename, config.get('ANNO_CODING'), ref_file, 'Coding region')
    dummy, exon = find_annotation(filename, config.get('ANNO_EXONS'), ref_file, 'Exon')
    dummy, intron = find_annotation(filename, config.get('ANNO_INTRONS'), ref_file, 'Intron')
    dummy, gene, near_gene = find_annotation(filename, config.get('ANNO_GENES'), ref_file, 'Gene', near_bp=near_bp)
    dummy, dummy, intergenic = find_annotation(filename, config.get('ANNO_GENES'), ref_file, 'Gene', near_bp=near_bp, negative=True)
    return snps, coding, exon, intron, gene, near_gene, intergenic






def process(filename, snp_anno,config):
    """Check number of SNPs which are in or near genes"""
    # Export data as a BED file
    print('Processing %s' % filename)
    (disease, stat) = parse_filename(filename)
    data = SnpGraph(filename)
    #data.read_matlab_file('%s.mat' % filename)
    #data.read_pairs_csv('%s_scores_chiSq.txt')
    #data.read_pairs_csv('%s_sort_RoP_SB_G4_o2_1e6.txt' % filename, score_col=2)
    #RESULTS_FILE='/home/ilana/Documents/GWISSmallBC/breast_cancer_small_scores_chiSq.txt' #EDIT name
    data.read_pairs_csv(RESULTS_FILE, score_col=0)  # EDIT score_col 
    data.read_loci('%s.bim' % filename)
    print('Writing %s_ucsc.bed' % filename)
    data.export2bed('%s_ucsc.bed' % filename)
    del data
    res = classify_loci('%s_ucsc.bed' % filename,config)
    # This is the assumed return order for classify_loci
    all_anno = ['total','coding','exon','intron','gene','near gene','intergenic']
    for idx,anno in enumerate(all_anno):
        snp_anno.fill_value(res[idx], [anno, disease, stat],
                            ['Annotation', 'Data', 'Statistic'])

def process_all(directory,config):
    """Process all files in the directory"""
    snp_anno = NArray(['Data','Annotation','Statistic'])
    snp_anno.add_keys(['RoP'], 'Statistic')
    snp_anno.add_keys(['total','coding','exon','intron','gene','near gene','intergenic'], 'Annotation')

    all_files = ['%s/%s' % (loc, os.path.splitext(f)[0])
                 for f in os.listdir(loc)
                 if (f.endswith('.fam') and not f.endswith('test.fam'))]
    print('Processing...')
    print(all_files)
    for cur_file in all_files:
        (disease, stat) = parse_filename(cur_file)
        snp_anno.add_keys([disease], 'Data')
        print(disease)
        process(cur_file, snp_anno,config)
    print('rows=Data')
    print('columns=Annotation')
    print(snp_anno.keys('Data'))
    print(snp_anno.keys('Annotation'))
    print(snp_anno.slice('Statistic','RoP'))


def find_gene_statistics(data,snp_bed,disease,stat,config,window=10000): # specify window 
    """ find genes within window that match overlap with snps in bedfile
    also sets gene_names attribute in snp_graph object (data)"""
    
    snp_anno = NArray(['Data','Annotation','Statistic'])
    snp_anno.add_keys([stat], 'Statistic')
    snp_anno.add_keys([disease], 'Data')
    snp_anno.add_keys(['total','coding','exon','intron','gene','near gene','intergenic'], 'Annotation')
        
    gene_names_outfile=('%s_gene_names_%s.txt') % (disease,stat) # a file to write gene names


    res = classify_loci(snp_bed,config,window) # call without writing file for now
    #res = classify_loci(snp_bed_file,config,window,gene_names_outfile) 
    all_anno = ['total','coding','exon','intron','gene','near gene','intergenic']
    for idx,anno in enumerate(all_anno):
        snp_anno.fill_value(res[idx], [anno, disease, stat], ['Annotation', 'Data', 'Statistic'])
    
    # output snp_anno contents to a text file

if __name__ == '__main__':
    import os, sys
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print('Usage: python %s <dataset name without .fam> [config]' % sys.argv[0])
        exit(1)
    loc = sys.argv[1]
    if len(sys.argv) == 3:
        config = configure_from_file(sys.argv[2])
    else:
        config = configure_from_file('config_default.py')
    if os.path.isfile('%s.fam' % loc):
        process(loc,config)
    elif os.path.isdir(loc):
        process_all(loc,config)
    







