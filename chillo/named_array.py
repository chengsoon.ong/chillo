"""A thin wrapper around numpy arrays such that dimensions
and coordinates can be named. This makes it easier to access
slices and elements of the array without resorting to a
particular convention.

The idea is to have dictionary like interfaces.
and have the array expand as needed.

The code is not designed for efficiency, but ease of use.
"""
import os
from numpy import array, ones, zeros, empty
from numpy import flatnonzero
import json

class NArray(object):
    """A thin wrapper around numpy arrays such that dimensions
    and coordinates can be named. This makes it easier to access
    slices and elements of the array without resorting to a
    particular convention.

    The idea is to have dictionary like interfaces.
    and have the array expand as needed.

    The code is not designed for efficiency, but ease of use.
    """
    def __init__(self, dim_names=None, max_elem=10, is_object=True):
        if dim_names is None:
            self.dim_names = array(['dummy'])
        else:
            self.dim_names = array(dim_names)                # the name of each axis
        self.elem_names = []                                 # list of list of names of each coordinate
        self.max_elem = max_elem                             # size of the array
        for ix in range(len(self.dim_names)):
            self.elem_names.append([])
        if is_object:
            self.data = empty([max_elem for ix in self.dim_names], dtype=object)     # the contents
        else:
            self.data = zeros([max_elem for ix in self.dim_names])     # the contents

    @property
    def ndim(self):
        """Number of dimensions"""
        return self.data.ndim

    @property
    def shape(self):
        """The number of elements in each dimension"""
        cur_shape = []
        for names in self.elem_names:
            cur_shape.append(len(names))
        return tuple(cur_shape)

    @property
    def content(self):
        """Return the array corresponding to meaningful data"""
        last = self.shape
        slicer = ''
        for ix in last:
            slicer += ':%d,' % ix
        slicer = slicer[:-1]
        return eval('self.data['+slicer+']')

    def keys(self, dim_name):
        """Return the list of keys"""
        if dim_name not in self.dim_names:
            raise KeyError('%s not found' % dim_name)
        cur_dim = flatnonzero(self.dim_names==dim_name)[0]
        return self.elem_names[cur_dim]
    
    def num_keys(self, dim_name):
        return len(self.keys(dim_name))
        
    def add_key(self, elem_name, dim_name, warn=True):
        """Add element elem_name to dimension dim_name"""
        cur_dim = flatnonzero(self.dim_names==dim_name)[0]
        if len(self.elem_names[cur_dim]) >= self.max_elem:
            raise IndexError('Exceeding size of array, allocate more using max_elem')
        if elem_name in self.elem_names[cur_dim]:
            if warn:
                print('Element name %s already exists, not added' % elem_name)
        else:
            self.elem_names[cur_dim].append(elem_name)

    def add_keys(self, elem_list, dim_name):
        """Add all elements in elem_list to dim_name"""
        for elem in elem_list:
            self.add_key(elem, dim_name)

    def fill_value(self, value, elem_names, dim_names):
        """Insert value into the array
        indexed by elem_names"""
        assert len(elem_names)==len(dim_names)
        assert len(dim_names)==self.ndim

        pointer = ''
        for (ix,dim) in enumerate(self.dim_names):
            cur_dim = flatnonzero(array(dim_names)==dim)[0]
            cur_elem = flatnonzero(array(self.elem_names[ix])==elem_names[cur_dim])[0]
            if cur_elem >= self.max_elem:
                raise IndexError('Exceeding size of array')
            pointer += '%d,' % cur_elem
        pointer = pointer[:-1]
        exec('self.data['+pointer+'] = value')

    def get_value(self, elem_names, dim_names):
        """Return the value from array"""
        assert len(elem_names)==len(dim_names)
        assert len(dim_names)==self.ndim

        pointer = ''
        for (ix,dim) in enumerate(self.dim_names):
            cur_dim = flatnonzero(array(dim_names)==dim)[0]
            cur_elem = flatnonzero(array(self.elem_names[ix])==elem_names[cur_dim])[0]
            pointer += '%d,' % cur_elem
        pointer = pointer[:-1]
        exec('value = self.data['+pointer+']')
        return locals()['value']

    def _slice(self, dim_name, dim_value):
        """Return a slice of the array,
        i.e. conditioning on dim_name = dim_value

        Base case
        """
        try:
            cur_dim = flatnonzero(self.dim_names==dim_name)[0]
        except IndexError:
            msg = 'Dimension name unknown: %s not found in ' % dim_name
            msg += str(self.dim_names)
            raise IndexError(msg)
        
        try:
            cur_val = flatnonzero(array(self.elem_names[cur_dim])==dim_value)[0]
        except IndexError:
            msg = 'Index name %s not found in dimension %s\n' % (dim_value, dim_name)
            msg += 'Known options:'
            msg += str(self.elem_names[cur_dim])
            raise IndexError(msg)
        slicer = ''
        for ix in range(self.ndim):
            if ix == cur_dim:
                slicer += str(cur_val)+','
            else:
                slicer += ':,'
        slicer = slicer[:-1]
        return eval('self.content['+slicer+']')

    def slice(self, dim_name, dim_value):
        """Return a slice of the array,
        i.e. conditioning on dim_name = dim_value
        """
        if type(dim_name) == type(''):
            return self._slice(dim_name, dim_value)
        elif type(dim_name) == type(list()) and len(dim_name)==1:
            return self._slice(dim_name[0], dim_value[0])
        else:
            assert len(dim_name)==len(dim_value)
            cur_dim = []
            cur_val = []
            for ix in range(len(dim_name)):
                try:
                    idx = flatnonzero(self.dim_names==dim_name[ix])[0]
                except IndexError:
                    msg = 'Dimension name unknown: %s not found in ' % dim_name[ix]
                    msg += str(self.dim_names)
                    raise IndexError(msg)
                cur_dim.append(idx)
                try:
                    val = flatnonzero(array(self.elem_names[idx])==dim_value[ix])[0]
                except IndexError:
                    msg = 'Index name %s not found in dimension %s\n' % (dim_value[ix], dim_name[ix])
                    msg += 'Known options:'
                    msg += str(self.elem_names[idx])
                    raise IndexError(msg)
                cur_val.append(val)
            slicer = ''
            for ix in range(self.ndim):
                if ix in cur_dim:
                    idx = flatnonzero(array(cur_dim)==ix)[0]
                    slicer += str(cur_val[idx])+','
                else:
                    slicer += ':,'
            slicer = slicer[:-1]
            return eval('self.content['+slicer+']')

    def save(self, file_name, merge=False):
        """Save the named array in json format"""
        if merge and os.path.isfile(file_name):
            cur = NArray()
            cur.load(file_name)
            self.merge(cur)
        out = {}
        out['dim_names'] = self.dim_names.tolist()
        out['elem_names'] = self.elem_names
        out['max_elem'] = max(self.shape)
        out['content'] = self.content.tolist()
        fp = open(file_name, 'w')
        json.dump(out, fp, indent=4)
        fp.close()

    def load(self, file_name):
        """Load the named array from a file with json format"""
        fp = open(file_name, 'r')
        raw_data = json.load(fp)
        fp.close()
        self.dim_names = array(raw_data['dim_names'])
        self.elem_names = raw_data['elem_names']
        self.max_elem = raw_data['max_elem']
        self.data = array(raw_data['content'])

    def merge(self, other):
        """Overwrite non-zero elements in self"""
        assert((self.dim_names == other.dim_names).all())
        assert(self.elem_names == other.elem_names)
        assert(self.max_elem == other.max_elem)
        to_copy = self.content == 0.0
        self.content[to_copy] = other.content[to_copy].copy()

    def savemat(self, file_name):
        """Save the named array in matlab format"""
        from scipy.io import savemat
        
        out = {}
        out['dim_names'] = self.dim_names.tolist()
        out['elem_names'] = self.elem_names
        out['max_elem'] = max(self.shape)
        out['content'] = self.content.tolist()
        savemat(file_name, out, oned_as='column')
