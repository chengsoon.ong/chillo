"""
From the GWIS snp pairs, identify connected subgraphs.
Using hg19, map the snps to genes. Display resulting subgraphs.

Gene annotations obtained from:
http://genome.ucsc.edu/cgi-bin/hgTables
table: "knownGenes" of hg19
"""
import os
import pybedtools as bt
from chillo.snp_graph import GeneGraph, SnpGraph
from chillo.ld_functions import calc_LD
from chillo.networkx_tools import view_multigraph_spring, view_graph_spring
import networkx as nx
from matplotlib.pyplot import show, savefig
from numpy import zeros, unique, flatnonzero, where
import numpy as np
import tables 
from matplotlib.pyplot import show, savefig, colorbar
import matplotlib.pyplot as plt
from math import log as mlog

from scipy.sparse.lil import lil_matrix # remove (in view_snp_subgraph temporarily because of community_id inconsistency in extracting subgraphs)

def process(data, config, stat, near_genes=100000, cluster=100000):
    """
    From the GWIS snp pairs, identify connected subgraphs.
    Using hg19, map the snps to genes. Display resulting subgraphs.

    Gene annotations obtained from:
    http://genome.ucsc.edu/cgi-bin/hgTables
    table: "knownGenes" of hg19
    """
    file_scores, file_loci, out_file = get_filenames(data, stat)
    # Export data as a BED file
    data = SnpGraph(file_scores, bonferroni=0.0)
    print('Read %s' % file_scores)
    data.read_pairs_csv(file_scores, max_pairs=1234)
    print('Read %s' % file_loci)
    data.read_loci(file_loci)
    data.export2bed('%s.bed' % out_file)
    del data

    # Cluster SNPs
    snps = bt.BedTool('%s.bed' % out_file)
    print('Number of SNPs: %d' % snps.count())
    clustered = snps.cluster(d=cluster)
    print('after clustering')
    print(clustered.head())
    clustered.moveto('clustered.bed')

    # Visualize the interaction graphs
    data = GeneGraph(file_scores, bonferroni=0.0)
    data.read_pairs_csv(file_scores, max_pairs=1234)
    data.read_loci(file_loci)
    data.read_bedfile('clustered.bed')

    # cleanup
    os.remove('clustered.bed')
    os.remove('%s.bed' % out_file)

    data.colour_connected()
    plot_large_components(data, out_file)
    #plot_clustered_subgraph(data, out_file, 1)
    plot_clustered_subgraph(data, out_file, 2)
    
## should be method of GeneGraph or SnpGraph
def plot_large_components(data, disease,stat, min_vertex=5, max_vertex=100):
    """Visualize the large graphs
       find subgraphs with more than min_vertex
    """
    out_file='%s_%s' % (disease,stat) # used for files
    print('Finding large components')
    (colours, counts) = data.find_large_components(min_vertex)
    print('Counts for largest connected subgraphs')
    print(colours)
    print(counts)
    for colour in colours:
        print('Visualizing %s_subgraph%02d' % (out_file, colour))
        subgraph = data.extract_subgraph(colour)
        if subgraph.num_vertex < max_vertex:
            view_multigraph_spring(subgraph,init=True)
            #subgraph.init_networkx()
            #plt.figure()
            #view_graph_spring(subgraph.graph)
            print('plot subgraph as multigraph view')            
            if out_file is not 'None':            
                savefig('%s_subgraph%02d.pdf' % (out_file, colour), dpi=300, orientation='landscape')

def plot_large_components_LD(data, disease, stat, hdf5file='None', filename='None', min_vertex=5, max_vertex=100,PLOT_LD=False,PLOT_R2=False):
    """plot subgraphs with edges as ld and/or r2"""
    out_file='%s_%s' % (disease,stat) # used for files
    print('Printing r2 or ld on edges of large components')
    (colours, counts) = data.find_large_components(min_vertex)
    for colour in colours:
        print('Visualizing %s_subgraph%02d' % (out_file, colour))
        subgraph = data.extract_subgraph(colour)
        subgraph.init_networkx()
        if subgraph.num_vertex < max_vertex:
            if (PLOT_LD==True):
                print('plot subgraph with edge labels as ld')
                if os.path.isfile('%s.bim' % filename): # must be directory with bim,bed,fam files
                    subgraph.graph_LD(ld='ld',PLINKdir=filename)  # plot graph with LD on edges
                    subgraph.read_graph_ld() # read in PLINK output file, ensure file exists first
                    plt.figure()
                    view_graph_spring(subgraph.graph)
                    plt.show()
                #view_multigraph_spring(subgraph_ld) # reinitialises LD currently, so not good
                    if out_file is not 'None':
                        savefig('%s_subgraph%02d_ld.pdf' % (out_file, colour), dpi=300, orientation='landscape')
            if (PLOT_R2==True):
                print('plot subgraph with edge labels as r2')
                if hdf5file is not 'None': # want to call PLINK with this file
                    subgraph.graph_LD(ld='r2',hdf5_file=hdf5file)  # plot graph with LD on edges, filename MUST contain
                    plt.figure()
                    view_graph_spring(subgraph.graph)
                    plt.show()
                    #view_multigraph_spring(subgraph_ld) # reinitialises LD currently, so not good
                    if out_file is not 'None':
                        savefig('%s_subgraph%02d_r2.pdf' % (out_file, colour), dpi=300, orientation='landscape')
            #base pair distance

def plot_large_components_gene_name(data, disease,stat, min_vertex=5, max_vertex=100):
    """plot subgraphs with node labels as gene names"""
    out_file='%s_%s' % (disease,stat) # used for files
    print('Printing gene name on nodes of large components')
    (colours, counts) = data.find_large_components(min_vertex)
    for colour in colours:
        print('Visualizing %s_subgraph%02d' % (out_file, colour))
        subgraph = data.extract_subgraph(colour)
        subgraph.init_networkx()
        if subgraph.num_vertex < max_vertex:
            print('plot subgraph with node labels:gene name')
            subgraph.graph_gene_name()  # plot graph with gene name on nodes
            #view_graph_spring(subgraph.graph)
            view_multigraph_spring(subgraph,init=False)
            if out_file is not 'None':
                savefig('%s_subgraph%02d_gene.pdf' % (out_file, colour), dpi=300, orientation='landscape')

def plot_large_components_bp_distance(data, disease,stat, min_vertex=5, max_vertex=100):
    """plot subgraphs with edge labels as bp distance"""
    out_file='%s_%s' % (disease,stat) # used for files
    print('Printing bp distance on edges of large components')
    (colours, counts) = data.find_large_components(min_vertex)
    for colour in colours:
        print('Visualizing %s_subgraph%02d' % (out_file, colour))
        subgraph = data.extract_subgraph(colour)
        subgraph.init_networkx()
        if subgraph.num_vertex < max_vertex:
            print('plot subgraph with edge labels as bp distance')
            subgraph = data.extract_subgraph(colour)
            subgraph.init_networkx() 
            graph_bp_distance(subgraph)  # plot graph with bp on edges
            #plt.clf()
            plt.figure()
            view_graph_spring(subgraph.graph)
            plt.show()
            if out_file is not 'None':
                savefig('%s_subgraph%02d_bp.pdf' % (out_file, colour), dpi=300, orientation='landscape')

def plot_clustered_subgraph(data, num_clus,out_file='None',min_vertex=5,max_vertex=100):
    """Visualize the subgraphs with num_clus number of clusters.
    If num_clus == 1, this means that all SNPs are local
    If num_clus == 2, this could indicate parallel interactions
    """
    # generate univariate and bivariate graphs 
    (colours, counts) = data.find_large_components(min_vertex) #find all large components
    #num_colour, counts = data.count_vertex_colour()
    #for colour in range(1,num_colour+1):
    for colour in colours:
        subgraph = data.extract_subgraph(colour)
        if len(unique(subgraph.bedline['clus_id'])) == num_clus:
            print('Subgraph %d has %d cluster(s)' % (colour, num_clus)) #item+1 is the colour name
            view_multigraph_spring(subgraph)
            if out_file is 'None': break # visualise
            else:
                savefig('%s_subgraph%02d_clus%d.pdf' % (out_file, colour, num_clus), dpi=300, orientation='landscape')

 

def view_subgraph_rs(data,rs_id,outfile,hub_only=False):
    """ view all subgraphs generated that contain rs_id
        if hub_only==True, then only display subgraph where rs is the hub"""
    snp_id = flatnonzero(data.snp['rs']==rs_id)[0] # get id from rs    
    subgraph = data.extract_snp_subgraph(snp_id)
    subgraph.community_id=lil_matrix((subgraph.num_vertex,subgraph.num_vertex)) # should remove and put in graph.py in correct function.
    # first view subgraph that contains snp_id as hub
    print('Visualizing %s_subgraph_hub_%02d' % (outfile, snp_id))
    view_multigraph_spring(subgraph,init=True)
    if outfile is not 'None':            
        savefig('%s_rshub_subgraph_hub_%sd.pdf' % (outfile, rs_id), dpi=300, orientation='landscape')
    
    if(hub_only==False):
        hub_list = data.find_hub_ids(min_hub_size=2) # find all snp_ids
        for snp_id in hub_list:
            subgraph = data.extract_snp_subgraph(snp_id)
            subgraph.community_id=lil_matrix((subgraph.num_vertex,subgraph.num_vertex))
            print('hub id is %s' % snp_id)
            if (rs_id in subgraph.snp['rs']):
                print('Visualizing %s_subgraph%02d' % (outfile, snp_id))
                view_multigraph_spring(subgraph,init=True)
                if outfile is not 'None':            
                    savefig('%s_subgraph_hub_%02d.pdf' % (outfile,snp_id), dpi=300, orientation='landscape')


def view_bipartite_subgraph(data,clus_id1,clus_id2,outfile='None'):
    """ plot specific subgraph between two clusters""" 
    subgraph=data.extract_bipartite_subgraph(clus_id1,clus_id2)
    print('Visualizing %s_subgraph%02d_%02d' % (outfile, clus_id1,clus_id2))
    view_multigraph_spring(subgraph,init=True)
    if outfile is not 'None':            
        savefig('%s_subgraph_bipartite_%02d_%02d.pdf' % (outfile,clus_id1,clus_id2), dpi=300, orientation='landscape')

def plot_large_bipartite_subgraph(data,outfile='None',min_edges=3):
    """for each bipartite subgraph with more than min_edges connecting any two nodes
       in each cluster
       """
    bipartite_nonzero = data.find_bipartite_subgraph(min_edges) # return list of cluster pairs, containing bipartite subgraph

    for idx in range(len(cl1.tolist())):
        clus_id_1=cl1[idx]
        clus_id_2=cl2[idx]
        if clus_id_1 <=clus_id_2: # indicies returned from a matrix with both upper and lower triangles filled
            subgraph=data.extract_bipartite_subgraph(clus_id_1,clus_id_2)
            print('Visualizing %s_subgraph%02d_%02d' % (outfile, clus_id_1,clus_id_2))
            view_multigraph_spring(subgraph,init=True)
            if outfile is not 'None':            
                savefig('%s_subgraph_bipartite_%02d_%02d.pdf' % (outfile,clus_id_1,clus_id_2), dpi=300, orientation='landscape')


def view_bipartite_subgraph_rs(data,rs_id,outfile='None'):
    """ plot all bipartite subgraphs where rs_id is a snp within subgraph
    """
    bipartite_nonzero = data.find_bipartite_subgraph() # return list of cluster pairs, containing bipartite subgraph
    cl1,cl2=bipartite_nonzero
    for idx in range(len(cl1.tolist())):
        clus_id_1=cl1[idx]
        clus_id_2=cl2[idx]
        if clus_id_1 <=clus_id_2: # indicies returned from a matrix with both upper and lower triangles filled
            subgraph=data.extract_bipartite_subgraph(clus_id_1,clus_id_2)
            if (rs_id in subgraph.snp['rs']):
                print('Visualizing %s_subgraph%02d_%02d' % (outfile, clus_id_1,clus_id_2))
                view_multigraph_spring(subgraph,init=True)
                if outfile is not 'None':            
                    savefig('%s_subgraph_bipartite_%02d_%02d_%02d.pdf' % (outfile,clus_id_1,clus_id_2,rs_id), dpi=300, orientation='landscape')

def summarise_bipartite_subgraphs(data,min_edges=3):
    """create a summary of bipartite subgraphs 
       cluster IDs: a tuple containing cluster names
       num eges: number of edges between cluster 1 and 2 
       (This does not include edges between vertices within a cluster
       clus1 snps: list of tuples for vertices in clus 1 with snp info [(rs,chrom,bp)]. 
       Same for clus 2 snps
       clus1 gene: list of tuples for vertices in clus 1 with gene info [(gene_names,bp_distance, snp upstream[-], or downstream [+] or gene)].   
       Same for clus2 genes
       """

    bipartite_nonzero = data.find_bipartite_subgraph(min_edges) # return list of cluster pairs, containing bipartite subgraph

    bipartite_subgraph_summary=zeros(len(bipartite_nonzero[0]),
       dtype={'names':['cluster IDs', 'num edges','clus1 snps','clus2 snps', 'clus1 genes', 'clus2 genes'], # finish
              'formats':[tuple,int,list,list,list,list]}) # array for set details
    cl1,cl2=bipartite_nonzero

    for idx in range(len(cl1.tolist())):
        clus_id_1=cl1[idx]
        clus_id_2=cl2[idx]
        if clus_id_1 <=clus_id_2: # indicies returned from a matrix with both upper and lower triangles filled
            bipartite_subgraph_summary[idx]['cluster IDs']=(clus_id_1,clus_id_2)
            bipartite_subgraph_summary[idx]['num edges']=data.bipartite_mat[clus_id_1-1,clus_id_2-1] 
            subgraph=data.extract_bipartite_subgraph(clus_id_1,clus_id_2)
            vertices_clus1=flatnonzero(subgraph.bedline['clus_id']==clus_id_1) # vertices from clus 1
            vertices_clus2=flatnonzero(subgraph.bedline['clus_id']==clus_id_2) # vertices from clus 2
            # keep only vertices in cluster with at least one non-zero edge to other cluster 
            # not interested in snps which connect to snps in same cluster
            bi_adj_mat=subgraph.adj_mat[vertices_clus1,:][:,vertices_clus2] 
            vertices_cl1=vertices_clus1[unique(bi_adj_mat.nonzero()[0])]
            vertices_cl2=vertices_clus2[unique(bi_adj_mat.nonzero()[1])]
            bipartite_subgraph_summary[idx]['clus1 snps']=subgraph.snp[vertices_cl1]
            bipartite_subgraph_summary[idx]['clus2 snps']=subgraph.snp[vertices_cl2]
            bipartite_subgraph_summary[idx]['clus1 genes']=subgraph.gene_list[vertices_cl1]
            bipartite_subgraph_summary[idx]['clus2 genes']=subgraph.gene_list[vertices_cl2]

    bipartite_subgraph_summary.sort(order='num edges')
    bipartite_subgraph_summary=bipartite_subgraph_summary[::-1]    
    return bipartite_subgraph_summary


def summarise_hub_subgraphs(data,min_edges=3):
    """create a summary of hub subgraphs 
       hub ID: 
       num hub targets:
       hub snp: list of tuples hub snp [(rs,chrom,bp)] (list is size 1) 
       target snps: ""
       hub gene: list of tuples for hub snp containing gene info [(gene_names,bp_distance, snp upstream[-], or downstream [+] or gene)].    
       target genes: Same for target snps
       """
    hub_list = data.find_hub_ids(min_hub_size=2) # find all snp_ids

    hub_subgraph_summary=zeros(len(hub_list),
       dtype={'names':['hub ID', 'hub num targets','hub snp','target snps', 'hub gene', 'target genes'], # finish
              'formats':[int,int,list,list,list,list]}) # array for set details

    for idx,snp_id in enumerate(hub_list):
        subgraph = data.extract_snp_subgraph(snp_id)
        print('hub id is %s' % snp_id)
        hub_subgraph_summary['hub ID'][idx]=snp_id
        hub_subgraph_summary['hub num targets'][idx]=data.diagonal_unweighted[snp_id]
        subgraph_hub_id=flatnonzero(subgraph.snp['rs']==data.snp['rs'][snp_id]) # might be non-unique match, not great
        target_snp_ids=flatnonzero(subgraph.snp).tolist()
        target_snp_ids.remove(subgraph_hub_id[0])
        hub_subgraph_summary[idx]['hub snp']=subgraph.snp[subgraph_hub_id]
        hub_subgraph_summary[idx]['target snps']=subgraph.snp[target_snp_ids]
        hub_subgraph_summary[idx]['hub gene']=subgraph.gene_list[subgraph_hub_id]
        hub_subgraph_summary[idx]['target genes']=subgraph.gene_list[target_snp_ids]

    hub_subgraph_summary.sort(order='hub num targets')
    hub_subgraph_summary=hub_subgraph_summary[::-1]
    return hub_subgraph_summary


def graph_LD(data,ld='r2',hdf5_file=None,PLINKdir=None,probe_dtype=None):
    """ Takes a SNPGraph object, 
        call either calc_LD to calculates LD distance for each pair (ld='r2'), 
        or calls PLINK as a subprocess to calculate LD
        displays graph of GWIS edges, with LD distance as edge labels"""        
    data.init_networkx()    
    print('filename is %s' % PLINKdir)
    print('hdf5 file is %s' % hdf5_file)
    if ld is 'r2':
        for idx1,idx2 in data.graph.edges():
            r=calc_LD(data,idx1,idx2,ld='r2',hdf5_file=hdf5_file)        
            data.graph.edge[i][j]['weight']=r                                     
    elif ld is 'ld':  # call plink to get LD
        if PLINKdir is not None:
            for idx1,idx2 in data.graph.edges():
                r=calc_LD(data,idx1,idx2,ld='ld',hdf5_data=None,PLINKdir=PLINKdir,probe_dtype=probe_dtype)        
                data.graph.edge[idx1][idx2]['weight']=float(ld)
        else:
            print("empty PLINKdir given")
            return
    return

def graph_gene_name(sdata):
    """create networkx object for subgraph where node labels are data.gene_name()"""
    sdata.init_networkx()
    for idx in sdata.graph.nodes():
        delimeter='\n '
        sdata.graph.node[idx]['label']=delimeter.join(list([sdata.chroff[idx],sdata.gene_name[idx]])) 


def graph_bp_distance(sdata):
    """create networkx object for subgraph where edge weight is self.bp_distance()"""
    #sdata.init_networkx()
    bp=sdata.bp_distance() # pre_compute
    for i,j in sdata.graph.edges():
        if bp[i,j]==-1:
            sdata.graph.edge[i][j]['weight']=-1
        else:
            sdata.graph.edge[i][j]['weight']=round(mlog(bp[i,j],10),0) # display out of 10. max is 10.
    



def get_filenames(data, stat, base_dir='/Users/cheng.ong/Data/WTCCC/'):
    """The details of file names are collected here"""
    file_scores = '%sGWIS/%s/%sWTC_default_scores_%s.txt' % (base_dir, data, data, stat)
    file_loci = '%s%sWTC.bim' % (base_dir, data)
    out_file = '%s_%s' % (data, stat)

    print('Processing: %s and %s' % (file_loci, file_scores))
    return file_scores, file_loci, out_file


def process_wtccc():
    all_data = ['bd', 'cad', 'cd', 'ht', 'ra', 't1d', 't2d']
    stats = ['DSS', 'SS', 'chiSq', 'maxAcc']
    for data in all_data:
        for stat in stats:
            process(data, stat)
            
if __name__ == '__main__':
    process_wtccc()

    
