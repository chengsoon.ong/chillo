def write_GSEA_results(results_file,disease,stat,MSigDb_set_name,n=10):
    """write the top n gene sets in MSigDB_set_name"""
    out_file='%s_%s_pairs_overlap_%s' % (disease,stat,MSigDb_set_name)
    out=open(out_file,'w')
    delimiter='\t'
    out.write(delimiter.join(map(str,results_file.dtype.names)) + '\n')
    for i in range(n):
        out.write(delimiter.join(map(str,results_file[i])) + '\n')

def write_results_file(outfilename, results_array, n=30, delimiter='\t', topline=True):
    outfile = open(outfilename, 'wb')
    num_lines=results_array.shape[0]
    if topline is True:
        outfile.write(delimiter.join(map(str,results_array.dtype.names)) + '\n')
    for i in range(n):
        if i >= num_lines:
            break
        else:
            outfile.write(delimiter.join(map(str,results_array[i])) + '\n')
    outfile.close()

