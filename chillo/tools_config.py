"""Miscellaneous tools for running experiments"""

def configure_from_file(filename):
    """Configure the data"""
    config = dict()
    exec(compile(open(filename).read(), filename, 'exec'), config)
    config.pop('__builtins__') # remove builtins from dictionary
    return config


def get_overlap_filename(config, mode, gold_std):
    """Returns the file name for the JSON file containing
    the overlap data"""
    return '%s/%s_overlap_%s_%s.json' %\
        (config['result_dir'], config['expt_name'], mode, gold_std)

def get_avgscore_filename(config, dataset):
    """Returns the file name for the list of pairs
    ranked by averaging all the scores from all splits"""
    return '%s/%s_sel_%s_avgscore.txt' %\
        (config['result_dir'], dataset, config['hypo_test'])

