from scipy.stats import pearsonr
import subprocess
import tables
import os
import fnmatch
import numpy

def calc_LD(data,idx1,idx2,ld='ld',hdf5_file=None,PLINKdir=None,probe_dtype=None):
    """ calculate distance of geneotypes between 2 snps
    ld is an option:
    'ld' calls plink as a subprocess
       - probe_dtype should be the data type in the PLNK file
    'r'  calculates pearson product-moment correlation of genotypes
    'r2' is r squared """
    if ld=='ld':  # calculate LD with plink
        print('calculate LD with plink')   
        if probe_dtype is not None:
            snp1_rs=data.snp[probe_dtype][idx1]
            snp2_rs=data.snp[probe_dtype][idx2]
        else: 
            print('provide valid probe_dtype')
            return
        ldfile='plink_ld_%s_%s.txt' % (snp1_rs,snp2_rs)
        success=call_PLINK(PLINKdir,snp1_rs,snp2_rs,ldfile) # writes output to ldfile
        if (success==True):
            r=read_PLINK(ldfile)
            return r
        else: 
            print('failed to calculate LD')
            return 
    else: # calculate correlation coefficient
        if os.path.isfile(hdf5_file):
            hdf5_data = tables.openFile(hdf5_file,'r')
            snp_index=int(data.idx2snp[idx1]-1) # index of snp in hdf5 file
            snp_index2=int(data.idx2snp[idx2]-1)
            genotypes_SNP1=hdf5_data.root.examples[snp_index]
            genotypes_SNP2=hdf5_data.root.examples[snp_index2]
            hdf5_data.close()
            r=pearsonr(genotypes_SNP1,genotypes_SNP2)[0]   
            if (ld=='r'):
                return r     
            elif(ld=='r2'):
                return r**2 #pearson correlation squared = correlation coefficient
        else:
           print('please provide valid hdf5_file')
           return
    
def call_PLINK(PLINKdir, snp1_rs, snp2_rs, ldfile):
    """ calls PLINK to calculate LD between two SNPs
        reads in the result from PLINK stdout
        writes the plink result to an output file"""
    if PLINKdir==None:
        print('must provide a valid directory where bed, bim, fam files are located for PLINK to access')   
        return False 
    if os.path.isfile('%s.bim' % PLINKdir):              
        #ld = call function to calculate LD externally 
        p=subprocess.Popen('plink --bfile %s --ld %s %s' % (PLINKdir,snp1_rs,snp2_rs), stdout=subprocess.PIPE, shell=True)
        p.wait()
        result=p.stdout.read()
        #result=p.communicate()[0]
        file_out=open(ldfile,'w')
        file_out.write(result)
        return True
    else:
        print('must provide valid directory where bed, bim, fam files are located for PLINK to access')
        return False

def read_PLINK(ldfile):
    """ reads in ld_file containing PLINK results
    and returns LD value"""
    ld=numpy.nan # default is no LD value can be calculated
    if os.path.isfile(ldfile):
        csvfile=open(ldfile,'r')
        for line in csvfile:
            j=0
            while line[j] != '\n' and line[j].isspace() is True:
                j=j+1
            if len(line) > (j+4): # check 
                if line[j:j+4] == 'R-sq':
                    start=j+7 # jump to value
                    j=j+7
                    while line[j].isspace() is False: # move cursor along value
                        j=j+1
                    end=j
                    ld=line[start:end] # end is not inclusive
                    break
        return ld
    else:
        print('please provide a valid file containing PLINK output')
        return 

