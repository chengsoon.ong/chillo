""" from a SnpGraph object (subgraph) and a target gene-gene interaction network
    initialise objects for use with Replication/graph_concurrence.py
    Pass in to graph_concurrence to obtain similarity score
"""

import sys
sys.path.append("/home/ilana/Documents/epistasis/Replication/")

from chillo.tools_GSEA import unique_rows # need to create tools for finding unique_rows.
from chillo.tools_plink import count_lines
from numpy import nonzero, triu, transpose, hstack, unique, sort
from graph_concurrence import WVLGraph, concurrence


def compute_concurrence(data,config):
    """ compute concurrence for all subgraphs:
    hubs, bipartite, community with pathway graphs in config['pathway'])
    """
    hub_list = data.find_hub_ids(min_hub_size=2) # find all snp_ids
    concurrence_hubs=zeros(len(hub_list))

    # for connected subgraphs
    data.colour_connected()
    (colours, counts) = data.find_large_components(5)

    for path_file in os.listdir(config['pathway']):
        path_dict=path_to_dict('%s%s' % (config['pathway'],path_file)) 
        for snp_id in hub_list:
            subgraph = data.extract_snp_subgraph(snp_id)
            hub_dict=subgraph_to_dict(subgraph)
            concurrence_hubs=graph_similarity(hub_dict,path_dict) #graph similarity between subgraph and a pathway
        for colour in colours:
            print('colour is %d' % colour)
            subgraph=data.extract_subgraph(colour)
            connected_sub_dict=subgraph_to_dict(subgraph)
            concurrence_val=graph_similarity(connected_sub_dict,path_dict)
            print(concurrence_val)

# issues: empty genes '' are treated as 1 unique gene name. these should be missing values 

def graph_similarity(uniq,uniq_path): 
    """ computes concurrence for 2 input networks
        uses tools in Replication/graph_concurrence.py
        two arguments contain dictionaries representing gene-gene interactions
        key is a unique gene-gene pair, value is a score
        returns concurrence score between two networks"""

    # step 3. create two arrays of tuples for purpose of identifying gene names to create idx2gene
    gene_tuple=zeros(len(list(uniq.keys())),dtype={'names':['gene1','gene2'],'formats':['S16','S16']})
    for idx,key in enumerate(list(uniq.keys())):       
        gene_tuple['gene1'][idx]=key[0]
        gene_tuple['gene2'][idx]=key[1]

    path_tuple=zeros(count_lines(path_filename),dtype={'names':['gene1','gene2'],'formats':['S16','S16']})
    for idx,key in enumerate(list(uniq_path.keys())):       
        path_tuple['gene1'][idx]=key[0]
        path_tuple['gene2'][idx]=key[1]

    # Step 4. create idx2gene with unique genes from both snp network (gene_tuple) and pathway network (path_tuple)
    idx2gene=array([])
    idx2gene=sort(unique(hstack([gene_tuple['gene1'].tolist(), gene_tuple['gene2'].tolist(), path_tuple['gene1'].tolist(), path_tuple['gene2'].tolist()])))
    idx2gene=idx2gene.tolist()

    # quick check for intersection of genes
    idx2gene1=sort(unique(hstack([gene_tuple['gene1'].tolist(), gene_tuple['gene2'].tolist()])))
    idx2gene2=sort(unique(hstack([path_tuple['gene1'].tolist(), path_tuple['gene2'].tolist()])))
    c=idx2gene1.tolist()
    d=idx2gene2.tolist()
    c_set=Counter(c)
    d_set=Counter(d)
    overlap = list(c_set & d_set)
    print('overlap between snp graph genes and pathway genes is %d' % len(overlap))
    print('total number of genes in snp graph %d' % len(c_set))
    # len(overlap=128)
    # len(c_set)= 947 # so 128 genes found from out of 947 in total   

    # Step 4. Create triplets of vertex pairs and score (array) needed for WVLGraph object initialisation
    snp_array=dict_to_vertex_array(uniq,idx2gene)
    path_array=dict_to_vertex_array(uniq_path,idx2gene)

    # Step 5. initialise WVLGraphs:
    a = WVLGraph()
    b = WVLGraph()
    a.init_from_triples(snp_array)
    b.init_from_triples(path_array)

    # Step 6. find concurrence
    return concurrence(a,b)

def subgraph_to_dict(data):    
     # create a triple for the subgraph
    rows1,cols1=nonzero(data.adj_mat) # get indices
    rows=rows1[rows1<cols1]
    cols=cols1[rows1<cols1] # a way of doing triu without casting to array
    indices=rows,cols

    # Step 1: create gene-gene graph for snp network (network 1),
    # step a.  create array from tuples of 5 elements:
    # gene1 (closest gene to snp1 in data.gene_list),gene2,
    # bp_distance1 (bp distance to gene 1 to snp),bp_distance2,
    # score (score for pair)
    pair1=[] 
    pair2=[]
    pair3=[]
    pair4=[]
    pair5=[]

    for i in range(len(rows)): 
        gene1=data.gene_list['gene'][rows[i]][0][0] # get gene name matching to row value of indice
        gene2=data.gene_list['gene'][cols[i]][0][0] # get gene name matching to col value of indice
        bp_distance1=data.gene_list['bp_distance'][rows[i]][0][0] # get gene name matching to row value of indice
        bp_distance2=data.gene_list['bp_distance'][cols[i]][0][0] # get gene name matching to col value of indice
        score=data.adj_mat[rows[i],cols[i]]
        pair1.append(gene1)
        pair2.append(gene2)
        pair3.append(bp_distance1)
        pair4.append(bp_distance2)
        pair5.append(score)

    pairs_score=pair1,pair2,pair3,pair4,pair5
    arr_score=transpose(array(pairs_score)) # create numpy array
    
    # step b: find unique rows, so that each combination of (gene1,gene2) is only in uniq dictionary once (as a key)    
    uniq=unique_rows_score(arr_score,score_column=4) # return unique pairs of genes

    return uniq


def path_to_dict(path_filename):
    """Assumes path_filename points to a gene-gene interaction file in SIF file format"""
    # step a. create array of gene1,gene2,score
    pair1=[]
    pair2=[]
    pair3=[]
    path_file=zeros(shape=(count_lines(path_filename),3))
    csvfile=open(path_filename,'r')
    reader=csv.reader(csvfile,delimiter='\t')
    for i,line in enumerate(reader): # for each line
        pair1.append(line[0])
        pair2.append(line[2])
        pair3.append(1)
    
    pairs=pair1,pair2,pair3
    path_file=transpose(array(pairs))
    
    # step b: find unique rows, so that each combination of (gene1,gene2) is only in uniq dict once (as a key)   
    # May NOT be desired if you want multiple edges. 
    uniq_path=unique_rows_score(path_file,score_column=2)

    return uniq_path

def dict_to_vertex_array(pair_dict,idx2gene):
    gene_array=zeros(shape=(len(list(pair_dict.keys())),3))

    for idx,key in enumerate(list(pair_dict.keys())):
        gene_array[idx][0]=idx2gene.index(key[0])
        gene_array[idx][1]=idx2gene.index(key[1])
        gene_array[idx][2]=pair_dict[key]
    
    return gene_array

def unique_rows_score(arr, score_column):
    """ returns a dictionary with a key for each unique row in array
        value is score for the pair, or where two snps map to the same gene(s), the maximum score
        assumes arr has first two columns as gene names:
        score_column is an integer that specifies column where score is 
        """
    #similar function in tools_GSEA.py - can be merged
    uniq = dict()
    for row_line in arr:
        row = (tuple(row_line))[0:2] # get gene names
        #if row in uniq:
        if (row[0],row[1]) in uniq or (row[1],row[0]) in uniq: # allow for alternative snp-pair ordering e.g. GeneA,GeneB or GeneB,GeneA
            #uniq[row] += 1
            entries=arr[((arr[:,0]==row[0]) & (arr[:,1]==row[1])) | ((arr[:,0]==row[1]) & (arr[:,1]==row[0])),score_column]
            if (row[0],row[1]) in uniq: 
                uniq[(row[0],row[1])]=max(entries)  # not good code but need to avoid G1,G2 score and G2,G1 score
            else:
                uniq[(row[1],row[0])]=max(entries)
        else:
            uniq[row] = row_line[score_column] # entry 4 is score
    return uniq


