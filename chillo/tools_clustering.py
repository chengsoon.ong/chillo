"""Tools to perform link clustering"""

from collections import defaultdict
from operator import itemgetter
from chillo.link_clustering import swap


def read_edgelist_weighted_my(dic_input,delimiter=None,nodetype=str,weighttype=float):
    """same as read_edgelist_unweighted except the input file now has three
    columns: node_i<delimiter>node_j<delimiter>weight_ij<newline>
    and the output includes a dict `ij2wij' mapping edge tuple (i,j) to w_ij
    """
    adj = defaultdict(set)
    edges = set()
    ij2wij = {}
    for l in  range(len(dic_input)): 
        for c in  range(len(dic_input[l])): 
            #ni,nj,wij = nodetype(L[0]),nodetype(L[1]),weighttype(L[2]) # other columns ignored
            if dic_input[l][c]>0:
                ni,nj,wij = l,c,dic_input[l][c]
                if ni != nj: # skip any self-loops...
                    ni,nj = swap(ni,nj)
                    edges.add( (ni,nj) )
                    ij2wij[ni,nj] = wij
                    adj[ni].add(nj)
                    adj[nj].add(ni) # since undirected
    return dict(adj), edges, ij2wij



def read_edgelist_unweighted_my(dic_input, delimiter=None, nodetype=str):
    """reads two-column edgelist, returns dictionary
    mapping node -> set of neighbors and a list of edges
    """
    adj = defaultdict(set) # node to set of neighbors
    edges = set()
    num_rows, num_cols = dic_input.shape
    for l in  range(num_rows): 
        for c in  range(num_cols):
            if dic_input[l,c]>0:
                ni,nj = l,c
                if ni != nj: # skip any self-loops...
                    edges.add( swap(ni,nj) )
                    adj[ni].add(nj)
                    adj[nj].add(ni) # since undirected
    return dict(adj), edges

def edgeTOcomm(e2c):
    """writes the .edge2comm, .comm2edges, and .comm2nodes files"""
    # renumber community id's to be sequential, makes output file human-readable
    c2c = dict( (c,i+1) for i,c in enumerate(sorted(list(set(e2c.values())))) ) # ugly...
    list_output=[]
    # write edge2cid three-column file:
    for e,c in sorted(e2c.iteritems(), key=itemgetter(1)):
        list_output.append([e[0],e[1],c2c[c]]) #simetria
        list_output.append([e[1],e[0],c2c[c]]) #simetria
        #f.write( "%s%s%s%s%s\n" % (str(e[0]),delimiter,str(e[1]),delimiter,str(c2c[c])) )
    return list_output
    



