import numpy as np
from numpy import array,unique,zeros,triu,nonzero,transpose, argsort
import csv 
import collections
import scipy as sp
import scipy.stats

    

def gene_set_analysis(gene_list,gene_sets):
    """ searches for overlap between snp_genes and each set in gene_sets
    snp_genes is a list"""
    #set_names=zeros(self.num_vertex,dtype=(numpy.str_,16))
    # find length of file, can call wc as subprocess
    #p=subprocess.check_output('wc -l %s' % gene_sets,shell=True)
    #start=0
    #j=0
    #while p[j].isspace() is False:
        #j=j+1
    #end=j # grab number of lines in file
    
    with open(gene_sets,'r') as f:
        lines=f.readlines()    
        num_lines=len([l for l in lines])    
    #print num_lines
    set_details=zeros(num_lines,
           dtype={'names':['Set Name','Overlap', 'Fisher','Set Size','Set Fraction','Overlap Genes'],
                  'formats':['S64',int,float,int,float,list]}) # array for set details

    gene_list_filt1=list() # remove whitespace
    for i,item in enumerate(gene_list):
        if item[0][0]:
            gene_list_filt1.append(item[0][0]) # start by looking at closest gene
    gene_list_filt2=unique(gene_list_filt1)
    list_multiset=collections.Counter(gene_list_filt2)

    csvfile=open(gene_sets,'r')
    reader=csv.reader(csvfile,delimiter='\t')
    for i,line in enumerate(reader): # each_line is a gene set
        set_details['Set Name'][i]=line[0] # name of set
        g_set=line[2:]
        g_set_multiset=collections.Counter(g_set)
        overlap=list((list_multiset & g_set_multiset).elements())
        list_not_set=list((list_multiset - g_set_multiset).elements())
        g_set_not_list=list((g_set_multiset -list_multiset).elements())    
        set_details['Overlap'][i]=len(overlap)
        set_details['Overlap Genes'][i]=overlap
        # fisher test
        a=len(overlap)
        b=len(list_not_set)
        c=len(g_set_not_list)
        d=0
        x=([a,b],[c,d])        
        results=sp.stats.fisher_exact(x,alternative="greater")    
        set_details['Fisher'][i]=results[1]
        set_details['Set Size'][i]=len(line[2:]) # how many genes in set
        set_details['Set Fraction'][i]=len(overlap)/float(len(line[2:])) # how many genes in s

        set_details.sort(order='Overlap')
        set_details=set_details[::-1]
    
    return set_details    
    
def gene_set_pair_analysis(data,gene_sets,GSEA_test='rank_sum',all_genes=True):
    """ looks for overlap of both genes mapping each snp in pair with gene set"""
    """ Pass in GSEA_test flag"""
    with open(gene_sets,'r') as f:
        lines=f.readlines()
        num_lines=len([l for l in lines])  # can update to use chillo.tools_plink function count_lines

    set_details=zeros(num_lines,
        dtype={'names':['Set Name','Pair Overlap', 'Unique Pair Overlap', 
                'Pair Names','Set Size', 'UnPairDivSetSize','GSEA_test_score','GSEA_test_p_val'], 
                'formats':['S64',int,int,list,int,float,float,float]}) # array for set details

    # get nonzero indices of adj_mat array (pairs)
    adj_array=data.adj_mat.toarray() # numpy array
    adj_array_upper=triu(adj_array) #avoid including interactions twice
    ind=nonzero(adj_array_upper) # tuple containing indicies of nonzero elements
    row,col=ind # unpack tuple

    # get pairs of genes
    pair1=[] # create two lists
    pair2=[]
    pair3=[]

    sorted_indices=np.argsort(adj_array[row,col])
    # create sorted indices
    row_sort=row[sorted_indices]
    col_sort=col[sorted_indices]

    if(all_genes==True):
        for i in range(len(row_sort)):
            for j in range(len(data.gene_list['gene'][row_sort[i]][0])): 
                g1=data.gene_list['gene'][row_sort[i]][0][j]     
                    # do all possible pairings with g2
                if not g1 or g1 in data.gene_list['gene'][row_sort[i]][0][0:j]:
                    break
                for k in range(len(data.gene_list['gene'][col_sort[i]][0])): 
                    g2=data.gene_list['gene'][col_sort[i]][0][k]
                    if not g2 or g2 in data.gene_list['gene'][col_sort[i]][0][0:k]:
                        break
                    pair1.append(g1)
                    pair2.append(g2)
                    score=adj_array[row_sort[i],col_sort[i]]
                    pair3.append(score)
    else:
        for i in range(len(row_sort)):     
            g1=data.gene_list['gene'][row_sort[i]][0][0] # get gene name of first indice
            g2=data.gene_list['gene'][col_sort[i]][0][0] # get gene name of second indice
            score=adj_array[row_sort[i],col_sort[i]]
            pair1.append(g1)
            pair2.append(g2)
            pair3.append(score)


        #g1=data.gene_name[row_sort[i]].split(', ') # get gene name of first indice
        #g2=data.gene_name[col_sort[i]].split(', ') # get gene name of second indice
        #if g1[0].isspace() is False and g2[0].isspace() is False: # both have legitimate gene names
            #for k,gene1 in enumerate(g1): # for each gene in the genes matching the first location (gene_1)
                #for l,gene2 in enumerate(g2): # "" second location (gene_2)
                    #print gene1,gene2
                    #pair1.append(gene1)
                    #pair2.append(gene2)
                    #pair3.append(score)

    pairs=pair1,pair2 # create tuple by packing 2 lists
    arr=transpose(array(pairs)) # create numpy array
    uniq=unique_rows(arr) # return unique pairs of genes

    pairs_score=pair1,pair2,pair3
    arr_score=transpose(array(pairs_score)) # create numpy array

    csvfile=open(gene_sets,'r')
    reader=csv.reader(csvfile,delimiter='\t')
    for i,line in enumerate(reader): # each_line is a gene set
        set_details['Set Name'][i]=line[0] # name of set
        set_details['Pair Names'][i]=[] # initialise as empty list of overlapping pairs
        #print i,line[0]
        g_set=line[2:]
        g_set_multiset=collections.Counter(g_set)
        #for j in range(len(pair1)):
            #list_multiset=collections.Counter([pair1[j],pair2[j]])
        for key in list(uniq.keys()):
            pair_multiset=collections.Counter(key)
            overlap=list((pair_multiset & g_set_multiset).elements())
            #overlap_true=(len(overlap)==2) # if both genes overlap the set, then the pair is true
            #if len(overlap)==1:           
            if len(overlap)==2:
                print('2',overlap, i)
                set_details['Pair Overlap'][i]=set_details['Pair Overlap'][i]+uniq[key]
                set_details['Unique Pair Overlap'][i]=set_details['Unique Pair Overlap'][i]+1
                set_details['Pair Names'][i].append(key)
        set_details['Set Size'][i]=len(line[2:]) # how many genes in set
        set_details['UnPairDivSetSize'][i]=set_details['Unique Pair Overlap'][i]/float(len(line[2:]))
        if(GSEA_test is 'rank_sum'):
            (set_details['GSEA_test_score'][i],set_details['GSEA_test_p_val'][i])=rank_sum(arr_score,g_set,uniq)

    set_details.sort(order='Unique Pair Overlap')
    set_details=set_details[::-1]
    return set_details    

def rank_sum(arr_score,g_set,uniq):
    """ create new array from arr_score where each gene-gene pair appears only once"""
    """ calculate Wilcox rank sum score and p value for input ranked list of pairs"""
    """ currently calls ranksums because sample size is often <20 for pairs in a set"""
    """ future - can calls Mann-Whitney test to deal with ties and continuity correction"""
    g_set_multiset=collections.Counter(g_set)

    in_set_list=[]
    not_in_set_list=[]
    # create a new array that has only row per gene-gene pair with best snp p-value matching
    for key in list(uniq.keys()):
        score=arr_score[((arr_score[:,0]==key[0]) & (arr_score[:,1]==key[1])) | ((arr_score[:,0]==key[1]) & (arr_score[:,1]==key[0])),2][0] # index [0] gives the lowest (most top-scoring rank)
        #rank=numpy.where(((arr_score[:,0]==key[0]) & (arr_score[:,1]==key[1])) | ((arr_score[:,0]==key[1]) & (arr_score[:,1]==key[0])))[0][0] # give row index (first item in tuple) and first item (highest rank)
        #gene_1.append(key[0])
        #gene_2.append(key[1])
        #score_list.append(score)

        # do GSEA on set
        list_multiset=collections.Counter(key)
        overlap=list((list_multiset & g_set_multiset).elements())
        if len(overlap)==2:
            print(overlap,2)
            in_set_list.append(score)
        elif len(overlap)==0:
            not_in_set_list.append(score)
        else: 
            continue
    
    #mannwhitneyu supports continuity correction and ties, but sample not always >20, so use rank sum
    if len(in_set_list) is not 0:
        print(sp.stats.ranksums(array(in_set_list),array(not_in_set_list)))
    return sp.stats.ranksums(array(in_set_list),array(not_in_set_list))
    #gene_pairs_score=gene_1,gene_2,score_list
    #gene_array=transpose(array(gene_pairs_score))
     

def unique_rows(arr):
    """ returns a dictionary where each key is a unique row in arr, 
        and each value is the number of times the row appears in array"""
    uniq = dict()
    for row in arr:
        row = tuple(row)
        #if row in uniq:
        if (row[0],row[1]) in uniq or (row[1],row[0]) in uniq: # allow for alternative snp-pair ordering e.g. GeneA,GeneB or GeneB,GeneA
            if (row[0],row[1]) in uniq:
                uniq[(row[0],row[1])] += 1
            if (row[1],row[0]) in uniq:
                uniq[(row[1],row[0])] += 1
        else:
            uniq[row] = 1
    return uniq

def plot_gene_results(results_file,disease,stat,MSigDb_set_name,n):
    """ plot Overlap results as bar chart for top n scoring gene sets"""
    results_file.sort(order='Overlap')
    ordered_results=results_file[::-1]
    width=0.2
    plt.bar(arange(n),ordered_results['Overlap'][0:n],width)
    #plt.yrange(0,max(y_sort))
    plt.xlabel('Gene Set Name, Set Size and Overlap Fraction of Set Size')
    plt.ylabel('Overlap')
    plt.title('%s %s Overlap with MSigDb %s' % (disease,stat,MSigDb_set_name))
    
    set_name_size = []    
    for i in range(len(ordered_results)):
        set_name_size.append(ordered_results['Set Name'][i] + '\n' + numpy.str(ordered_results['Set Size'][i]) + ':' + trunc(ordered_results['Fraction'][i],2))

    plt.xticks(arange(n)+width/2.,set_name_size,rotation='horizontal',fontsize=5)
    #plt.yticks(range(data.num_vertex)[::100],tmp_chroff[::100],fontsize=5)
    plt.show    
    savefig('%s_%s_Overlap_%s.pdf' % (disease,stat,MSigDb_set_name), dpi=300, orientation='landscape')
    plt.clf()



def trunc(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    return ('%.*f' % (n + 1, f))[:-1]    
    
