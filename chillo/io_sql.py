"""Interface to sqlite database containing GWAS data"""

import os
from numpy import array, empty, flatnonzero
from numpy import uint8, ceil
from bitstring import Bits
import sqlite3


class GenotypeData(object):
    """Genotype data for genome wide association studies.
    Provides methods for connecting to the database
    and slicing probes.
    """
    def __init__(self, filename, verbose=False):
        self.verbose = verbose
        self.file_name = filename
        self.data_name = os.path.basename(filename)
        if not os.path.isfile('%s.db' % filename):
            print('%s.db not found' % filename)
            return
        self.db_name = '%s.db' % filename

        # The handle to the database
        self.db_file = None
        self.cursor = None

        # SNP lookup table
        self.snp_lookup = {}
        self.create_snp_lookup()

        # The data
        self.phenotype = array([])
        
    @property
    def num_probes(self):
        """The number of loci tested, the number of genotypes per invididual"""
        self.cursor.execute('SELECT COUNT(*) FROM probe WHERE encoding=1')
        return int(self.cursor.fetchone()[0])

    @property
    def num_individuals(self):
        """The number of examples, the number of individuals in study"""
        return len(self.phenotype)

    def open_file(self):
        """Connect to the database"""
        if self.verbose: print('Connecting to %s' % self.db_name)
        self.db_file = sqlite3.connect(self.db_name)
        self.db_file.text_factory = str
        self.cursor = self.db_file.cursor()
        self.init_phenotypes()

    def close_file(self):
        """Disconnect from the database"""
        if self.verbose: print('Closing connection to %s' % self.db_name)
        self.db_file.close()
        
    def init_phenotypes(self):
        """Extract the phenotype from the individuals"""
        if self.verbose: print('creating phenotype vector')
        phenotypes = read_phenotype(self.cursor)
        phenotypes[phenotypes==1] = -1
        phenotypes[phenotypes==2] = 1
        phenotypes.shape = (len(phenotypes),1)
        #check for undefined phenotype
        undefined_phenotype = flatnonzero(phenotypes==0)
        if len(undefined_phenotype) > 0:
            print('Some phenotypes were undefined')
            print(undefined_phenotype)
        self.phenotype = phenotypes

    def get_probe(self, idx_snp):
        """Return the probe information for snp idx_snp"""
        self.cursor.execute('SELECT id, name, chr, position, min, maj FROM probe WHERE id=%d' % (idx_snp+1))
        return self.cursor.fetchone()
        
    def get_genotype(self, idx_snp):
        """Return the array of genotypes for snp idx_snp.
        Note that SQL database has id which is 1 based.
        """
        self.cursor.execute('SELECT genotype FROM probe WHERE id=%d' % (idx_snp+1))
        cur_snp = self.cursor.fetchone()
        raw = str(cur_snp[0])
        cur_gen = []
        for ix_byte, cur_byte in enumerate(raw):
            cur_gen.extend(self.snp_lookup[cur_byte])
        return array(cur_gen, dtype=uint8)[:self.num_individuals]


    def get_idx_case(self):
        """Return the index of individuals who are cases"""
        return flatnonzero(array(self.phenotype) == 1)

    def get_idx_control(self):
        """Return the index of individuals who are controls"""
        return flatnonzero(array(self.phenotype) == -1)

    def create_snp_lookup(self):
        """Create a lookup dictionary to
        convert a single byte to 4 genotypes,
        first SNP is the right most two bits.

        Create PLINK like encoding:
        00 = Homo1
        01 = Heterozygote
        10 = Homo2
        11 = Missing
        """
        states = ['00','01','10','11']
        toplink = {'00':3,'01':0,'10':1,'11':2}
        for pos1 in states:
            for pos2 in states:
                for pos3 in states:
                    for pos4 in states:
                        key = Bits('0b'+pos4+pos3+pos2+pos1).bytes
                        geno = [toplink[pos1], toplink[pos2], toplink[pos3], toplink[pos4]]
                        self.snp_lookup[key] = geno

    def create_snp_lookup_orig(self):
        """Create a lookup dictionary to
        convert a single byte to 4 genotypes,
        first SNP is the right most two bits.
        """
        states = ['00','01','10','11']
        for pos1 in states:
            for pos2 in states:
                for pos3 in states:
                    for pos4 in states:
                        key = Bits('0b'+pos4+pos3+pos2+pos1).bytes
                        geno = [Bits('0b'+pos1).uint,
                                      Bits('0b'+pos2).uint,
                                      Bits('0b'+pos3).uint,
                                      Bits('0b'+pos4).uint
                               ]
                        self.snp_lookup[key] = geno

def _snp_lookup(cur_byte):
    """
    DEPRECATED: Use the lookup table instead (create_snp_lookup)
    
    Convert single byte to 4 genotypes,
    first SNP is the right most two bits.
    """
    raw = Bits(bytes=cur_byte)
    genotype = []
    for gen in raw.cut(2):
        genotype.insert(0, gen.uint)
    print genotype, snp_lookup[cur_byte]
    return genotype

def read_phenotype(cursor):
    """Read the phenotype and return as a numpy array"""
    cursor.execute('SELECT phenotype.plink FROM phenotype')
    raw = cursor.fetchall()
    phenotype = []
    for p in raw:
        phenotype.append(p[0]) 
    phenotype = array(map(int, phenotype))
    #num_cases = len(flatnonzero(phenotype == 1))
    #num_controls = len(flatnonzero(phenotype == 2))
    #print('%d cases, %d controls' % (num_cases, num_controls))
    #print('total = %d individuals' % len(phenotype))
    return phenotype

def get_num_probes(cursor):
    """Return the number of probes in the database"""
    cursor.execute('SELECT COUNT(*) FROM snp')
    num_probes = int(cursor.fetchone()[0])
    print('%d probes' % num_probes)
    return num_probes

def read_genotype(cursor, num_ind):
    """Read the genotype and return as a numpy array"""
    create_snp_lookup()
    num_probes = get_num_probes(cursor)
    rnum_ind = int(ceil((num_ind/4.0)))*4
    genotype = empty((num_ind, num_probes), dtype=uint8)
    cursor.execute('SELECT snp.genotype FROM snp')
    for ix in range(num_probes):
        cur_snp = cursor.fetchone()
        if ix%10000 == 0: print ix
        raw = str(cur_snp[0])
        cur_gen = []
        for ix_byte, cur_byte in enumerate(raw):
            cur_gen.extend(snp_lookup[cur_byte])
            # Potentially insert directly into array, but seems to be slower
            #genotype[4*ix_byte:(4*(ix_byte+1)), ix] = snp_lookup[cur_byte]
        genotype[:,ix] = array(cur_gen, dtype=uint8)[:num_ind]
    return genotype

if __name__ == '__main__':
    data_name = '/Users/cheng.ong/Data/GWAS/celUK1b'
    data = GenotypeData(data_name, verbose=True)
    data.open_file()
    data.init_phenotypes()
    print('%d probes, %d individuals' % (data.num_probes, data.num_individuals))
    print('%d cases, %d controls' % (len(data.get_idx_case()), len(data.get_idx_control())))
    print data.get_genotype(1)
    print data.get_genotype(42)
