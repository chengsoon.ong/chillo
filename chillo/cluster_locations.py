import pybedtools as bt
from pybedtools.contrib import Classifier

def find_clusters(data,snp_bed_file):
    """ Cluster SNPs using locations in snp_bed_file and write to clustered.bed"""
    snps = bt.BedTool(snp_bed_file)
    snps=snps.sort() # must be sorted on chrom,bp or cluster complains
    print('Number of SNPs: %d' % snps.count())
    cluster=100000
    clustered = snps.cluster(d=cluster)
    print('after clustering')
    print(clustered.head())
    clustered.moveto('clustered.bed')

