"""Functions for computing overlap, such as intersections and unions"""
import pybedtools as bt
from numpy import array, unique, absolute, minimum, zeros

def find_gene_intersection(snp_file,config,left_window=0,right_window=0):
    """ find intersection of snp_file (a BED file) and RefFlat.txt file
        return the original gene annotations plus snp bedtool info
    """
    ref_file=config.get('ANNO_REFFLAT') # contains RefSeq gene ids
    ref = bt.BedTool(ref_file)

    snps = bt.BedTool(snp_file)
    bt_intersect_results=bedtools_intersection(snps,ref,left_window,right_window)
    return bt_intersect_results

def snp_region_intersection(snp_file,region_file,left_window=0,right_window=0):
    """ find intersection of snp_file (a BED file) and a second bed file with chromosome regions defined
        return the original gene file columns plus matching snps"""
    snps = bt.BedTool(snp_file) 
    region = bt.BedTool(region_file)
    bt_intersect_results=bedtools_intersection(snps,region,left_window,right_window)
    return bt_intersect_results


def bedtools_intersection(snp_bedtool,region_bedtool,left_window=0,right_window=0):
    """ finds intersection or window between two bedtools
        intersect - looks for snp location within region start and stop sites 
        window - looks for snp location upstream or downstream, using left and right windows
        sw=True option enforces strandedness - that is 
        - if gene is on forward strand, 
        ----look for left_window distance upstream (lower coordinate) and right downstream.  
        - if gene is on reverse strand (-), 
        ----look for left_window downstream (higher coordinate) and right upstream"""
    if left_window == 0 and right_window == 0:    
        inside = region_bedtool.intersect(snp_bedtool,wa=True,wb=True)
    else:
        inside = region_bedtool.window(snp_bedtool, l=left_window,r=right_window, sw=True)
    
    snp_chrom=region_bedtool.field_count()+0 # snp_chrom is 1st col of snp_bedtool
    snp_start=region_bedtool.field_count()+1 # snp_start is 2st col of snp_bedtool
    snp_stop=region_bedtool.field_count()+2 # 
    rs=region_bedtool.field_count()+3 # 

    results_array=zeros(inside.count(), dtype={'names':['chrom', 'chromStart', 'chromEnd', 'rs', 'gene_chrom', 'gene_start', 'gene_stop', 'gene', 'gene strand', 'distance', 'absolute distance'],'formats':['S16', int,int,'S16','S16',int,int,'S16','S16','S16',int]})

    if region_bedtool.field_count() > 4:
        for idx,f in enumerate(inside):
            snp_distance=_gene_snp_distance(f[snp_stop],f[1],f[2],gene_strand=f[5])
            results_array[idx] = (f[snp_chrom],f[snp_start],f[snp_stop],f[rs],f[0],f[1],f[2],f[3],f[5], snp_distance[0],snp_distance[1])
    else:
        for idx,f in enumerate(inside):
            snp_distance=_gene_snp_distance(f[snp_stop],f[1],f[2],gene_strand=None) # no strand information
            results_array[idx] = (f[snp_chrom],f[snp_start],f[snp_stop],f[rs],f[0],f[1],f[2],f[3],'', snp_distance[0],snp_distance[1])

    unique_results_array=unique(results_array) # some lines appear twice, still retain lines with same gene, but different gene start/stop locations


    return unique_results_array





def _gene_snp_distance(snp_site,gene_start,gene_stop,gene_strand=None):
    """return distance between snp_site and either gene_start or gene_stop (minimum value)
        input arguments are fields from a line from bedtools
        returns a tuple of 
        - signed distance, indicating snp located upstream(-) or downstream(+) from gene
        - absolute distances
        If snp is within gene, return 0 for both values
        If no strand given (gene_strand=None), return blank for string""" 
    start_distance=int(gene_start)-int(snp_site)
    stop_distance=int(gene_stop)-int(snp_site)
    if int(gene_start) < int(snp_site) < int(gene_stop):
        #return 'snp within gene'
        bp_distance='0'
        absolute_bp_distance=0
    elif absolute(start_distance) < absolute(stop_distance): # start site is closest 
        absolute_bp_distance=absolute(start_distance) # start_distance should be +ve number
        if(gene_strand=='+'): 
            bp_distance='-%d' % absolute_bp_distance  # -ve as upstream distance
        elif(gene_strand=='-'): 
            bp_distance='+%d' % absolute_bp_distance #  +ve value as downstream distance  
        elif(gene_strand==None):
            bp_distance='no strand' % absolute_bp_distance
    else: # stop site is closest
        absolute_bp_distance=absolute(stop_distance) # should be -ve number
        if(gene_strand=='+'): 
            bp_distance='+%d' % absolute_bp_distance  # +ve as downstream distance
        elif(gene_strand=='-'): 
            bp_distance='-%d' % absolute_bp_distance #  -ve value upstream distance
        elif(gene_strand==None):
            bp_distance='no strand' % absolute_bp_distance

    return bp_distance,absolute_bp_distance # return negative value as upstream distance





