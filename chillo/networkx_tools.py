"""Visualization using NetworkX"""

import matplotlib.pyplot as plt
import networkx as nx

def view_graph_spring(G, sizes=None):
    """Use spring plots to visualize graph"""
    #pos = nx.pydot_layout(G)
    pos = nx.spring_layout(G)
    node_labels = dict([(u,d['label']) for u,d in G.nodes(data=True)])
    if sizes is not None:
        nx.draw_networkx_nodes(G, pos, alpha=0.7, node_size=sizes)
    else:
        nx.draw_networkx_nodes(G, pos, alpha=0.7)
    nx.draw_networkx_labels(G, pos, node_labels)
    nx.draw_networkx_edges(G, pos,width=2, alpha=0.5, edge_color='black')
    edge_labels = dict([((u,v,),'%2.1f'%d['weight']) 
                        for u,v,d in G.edges(data=True)]) 
    nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels, label_pos=0.3) 

def view_multigraph_spring(G,init=True): # flag if network needs initialising
    #"""Visualize graph before and after merging vertices"""
    plt.figure(figsize=(12,5))
    if init is True:    
        G.init_networkx()
    #if gene_name is True:                
        #G.graph_gene_name() # write names of genes on the node
    plt.subplot(121)
    view_graph_spring(G.graph, 100*G.num_snps)
    frame = plt.gca()
    frame.get_xaxis().set_visible(False)
    frame.get_yaxis().set_visible(False)
    G.merge_vertices()
    plt.subplot(122)
    view_graph_spring(G.graph, 100*G.num_snps)
    frame = plt.gca()
    frame.axes.get_xaxis().set_visible(False)
    frame.axes.get_yaxis().set_visible(False)
