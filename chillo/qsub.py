"""Generate a file for submitting via qsub"""

import subprocess

def gen_header(name, queue, hours, cores=4, mem=2000):
    """Generate the header for qsub"""
    header = '#!/bin/bash\n#PBS -N %s\n' % name
    header += '#PBS -A pMelb0105\n'
    header += '#PBS -q %s\n#PBS -l nodes=1:ppn=%d\n' % (queue, cores)
    header += '#PBS -l walltime=%d:59:00\n' % (hours-1)
    header += '#PBS -l pmem=%dmb\n' % mem
    return header

def gen_header_massive(name, hours=24, mem=6000):
    """Generate the header for qsub on massive"""
    header = '#!/bin/bash\n'
    header += '#PBS -S /bin/bash\n'
    header += '#PBS -N %s\n' % name
    header += '#PBS -l nodes=1:ppn=1:gpus=1\n'
    header += '#PBS -l pmem=%dMB\n' % mem
    header += '#PBS -l walltime=%d:59:00\n' % (hours-1)
    header += 'cd $PBS_O_WORKDIR\n'
    return header

def submit(header, command):
    """Concatenate header and command and qsub it"""
    script = open('temp.sh', 'w')
    script.write(header)
    script.write(command+'\n')
    script.close()
    subprocess.check_call('qsub temp.sh', shell=True)


# Testing code
def gen_command(process):
    """The actual command to be executed"""
    command = 'python3 '
    command += '/home/cong/Nicta/epistasis/Replication/sim_replication.py '
    command += '/home/cong/Nicta/epistasis/Replication/config_cheng.py '
    command += process
    command += '\n'
    return command

def qsub(process, name, queue, hours):
    script = open('temp.sh', 'w')
    text = gen_header(name, queue, hours)
    text += gen_command(process)
    script.write(text)
    script.close()
    subprocess.check_call('qsub temp.sh', shell=True)

if __name__ == '__main__':
    qsub('submit', 'bcsmall', 'fast', 1)
