
#datasets = ['bdWTC', 'cadWTC', 'cdWTC', 'htWTC', 'raWTC', 't1dWTC', 't2dWTC',
#            'bdWTCp','cadWTCp','cdWTCp','htWTCp','raWTCp','t1dWTCp','t2dWTCp']
datasets = ['bdWTC', 'cadWTC', 'cdWTC', 'htWTC', 'raWTC', 't1dWTC', 't2dWTC']

# PLINK bed, bim, fam files
genotype_dir = '/Users/cheng.ong/Data/WTCCC/'
#genotype_dir = '/home/projects/pMelb0105/Data/WTCCC/'

# GWIS results
gwis_dir = '/Users/cheng.ong/temp/results_gwis/'
#gwis_dir = '/home/projects/pMelb0105/gwis-results/'
hypo_test = ['GSS']

# File containing Affymetrix ID, RS number and chromosome basepair location
rs_map = '/Users/cheng.ong/Data/UCSC/snpArrayAffy250NspSty.txt'

