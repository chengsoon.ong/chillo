
# set for breast cancer, or set to empty if not needed
DISEASE_SUBTYPE=''
#DISEASE_SUBTYPE='_ERplus'
#DISEASE_SUBTYPE='_ERminus'

# PLINK bed, bim, fam files
genotype_dir = '/home/ilana/Documents/GWISSmallBC/VLSCI/bc571/PLINK/'

output_dir = '/home/ilana/Documents/GWISSmallBC/results/'

bc_genes_dir='/home/ilana/Documents/GWISSmallBC/Mirek_gene_names/'
    
# Files from UCSC genome browser
DATA_DIR = '/home/ilana/Documents/UCSC/'
BUILD = 'hg19'
ANNO_GENES = DATA_DIR + BUILD + 'genes.bed.gz'
ANNO_CODING = DATA_DIR + BUILD + 'coding.bed.gz'
ANNO_EXONS = DATA_DIR + BUILD + 'exons.bed.gz'
ANNO_INTRONS = DATA_DIR + BUILD + 'introns.bed.gz'
ANNO_MIRNA = DATA_DIR + BUILD + 'sno_miRNA.bed.gz'
ANNO_REFFLAT = DATA_DIR + BUILD + 'refFlat.bed.gz'	

# File containing Affymetrix ID, RS number and chromosome basepair location
#rs_map = ucsc_dir + 'snpArrayAffy250NspSty.txt'
rs_map = DATA_DIR # UCSC directory contains SNP137Common table downloads from UCSC for uploaded SNP list (different for each subtype and stat)


RS_LOCATION_bc571_DSS_cols=(0,2,3) # storing chromosome base pair end (column 2) as location

# GWIS results
#gwis_dir = '/Users/cheng.ong/temp/results_gwis/'
gwis_dir = '/home/ilana/Documents/GWISSmallBC/VLSCI/bc571/'
stat = 'DSS'
num_pairs=4000

HDF5_FILE='/home/ilana/Documents/GWISSmallBC/VLSCI/bc571/bc571_mat.h5'

#GSEA 
MSigDB_dir='/home/ilana/Documents/MSigDb/'

# pathway analysis
pathway = '/home/ilana/Documents/PathwayCommons_NCINature_SIF/'
