import sys
sys.path.append('/home/ilana/Documents/epistasis/GraphEnrich')
from chillo.tools_config import configure_from_file
from chillo.snp_graph import GeneGraph
from chillo.tools_plink import count_lines

#from snp_graph import SnpGraph # initialising a GeneGraph subclass
#from gene_graph import GeneGraph
#from config_default import configure_from_file # will move to new location
import os
from chillo.tools_overlap import snp_region_intersection
from chillo.tools_summary_plots import draw_heatmap, plot_hist,plot_bp_vs_ld
from chillo.cluster_locations import find_clusters
from chillo.check_genes import find_gene_statistics 
from chillo.tools_GSEA import gene_set_analysis, gene_set_pair_analysis, plot_gene_results
from chillo.write_ndarray_results import write_results_file
from chillo.view_subgraphs import plot_clustered_subgraph, plot_large_components, plot_large_components_LD, plot_large_components_gene_name, plot_large_components_bp_distance, view_bipartite_subgraph, view_subgraph_rs, view_bipartite_subgraph_rs, summarise_bipartite_subgraphs,summarise_hub_subgraphs, plot_large_bipartite_subgraph
    
from read_bc_genes import read_breast_cancer_genes, export_genes_2bed

def process(disease_main, config):
    """ processes bc571 data set for stats DSS and GSS
    disease_names specifies name of disease. There should be a folder with this name in the output directory to write results i.e. config['output_dir'])
    generate heatmaps, histograms and other summarisation plots
    find overlapping functional regions by type (e.g. intron) from UCSC flat files
    find gene names within 100,000bp of snp
    perform gene set enrichment and gene set pair enrichment and produce report
    generate and view all subgraphs with edges labelled, 
    and produce reports on hub subgraphs, bipartite subgraphs
    Note: If no up-to-date snp location file exists, run data.export_snp_list('%s_snp_list_%s.txt' % (disease,stat)) to export snp rs ids and then upload to UCSC table browser and download bedfile from table browser using Snp137Common primary table. 
    """    
#filename='/home/ilana/Documents/GWISSmallBC/VLSCI/bc571/bc571'
    config = configure_from_file('config_bc571.py')

    print disease_main    
    #file=filename.split('/')[-1]
    subtype=config.get('DISEASE_SUBTYPE') # may be empty in config file
    disease='%s%s' % (disease_main,subtype) # subtype useful for breast cancer data
    #print disease

    results_dir=('%s%s/%s' % (config['output_dir'],disease,config['stat']))

    if os.path.isdir(results_dir): # os.getcwd() gives current working directory
        os.chdir(results_dir) # change into subdirectory e.g. bd
    else:
        print('Usage: please specify in config file results top level directory, with subdirectories for disease/stat')
        exit(1)

    # replace with list in config file
    # for stat in list(['chi2','SS','DSS']):
    # replace with call to collect info - ask Cheng about this

    data = GeneGraph(disease_main)
    bim_file = config['genotype_dir'] + disease_main + '.bim' # BIM file
    num_snps = count_lines(bim_file)
    stat=config['stat']
    num_pairs=config['num_pairs']
    data.set_bonferroni(config['stat'],num_snps,bonferroni=0)

    # initialise results file (containing top pairs) and rs_location file (containing updated location details)    
    gwis_res = '%s%s_scores_%s.txt' % (config['gwis_dir'], disease, config['stat']) # results_file
    rs_location_file='%sSNP137Common_%s_%s_top%d.bed' % (config['rs_map'], disease,stat,num_pairs) # location file


    
    # do double pass on read_pairs_csv and read_loci 
    # pairs_not_found contains snps for pairs not found in RS_LOCATION flat file. 
    # I think for GSS or final data analysis set, it is best to look up missing pairs manually using lift-over tool or dbSnp. DSS: rs2127820, rs6028723 not found

    all_pairs = data.read_gwis_file(gwis_res,score_col=2,max_pairs=num_pairs,line_start=0,delim=',') 
    pairs_not_found=data.initialise_snps_and_sparse_graph(all_pairs,stat,bim_file,rs_location_file,(0,2,3)) # 
    #data.export_snp_list('bc571_pairs.txt')
    new_pairs=data.remove_missing_snps(all_pairs=all_pairs,remove_snp_list=pairs_not_found)  
    pairs_not_found=data.initialise_snps_and_sparse_graph(new_pairs,stat,bim_file,rs_location_file,(0,2,3)) # 

    if len(pairs_not_found) >0:    
        print 'error: some snps still have no match in affy flat file'
        exit(1)

    data.set_adj_mat(config['stat'])

    print("export snps as bed file")
    out_file=('%s_bedfile_%s') % (disease,config['stat'])
    data.export2bed('%s.bed' % out_file)
    snp_bed= '%s.bed' % out_file 

    print("read gene names")
    #data.read_gene_names(snp_bed,config,left_window=2000,right_window=500) # windows from dbSnp FAQ
    data.read_gene_names(snp_bed,config,left_window=100000,right_window=100000) 

    print("gene set analysis")
    for gene_sets in os.listdir(config['MSigDB_dir']):
        print('processing %s' % gene_sets)
        gene_sets_loc='%s/%s' % (config['MSigDB_dir'],gene_sets)        
        if os.path.isfile(gene_sets_loc) and gene_sets.split('.')[-1] == 'gmt':
            results=gene_set_analysis(data.gene_list['gene'],gene_sets_loc)
            genes_out_file='%s_%s_genes_overlap_%s' % (disease,stat,gene_sets)
            write_results_file(genes_out_file,results)
            #plot_gene_results(results,disease,stat,gene_sets,n=10) 
            
            results_pair=gene_set_pair_analysis(data,gene_sets_loc,GSEA_test='False')
            pairs_out_file='%s_%s_pairs_overlap_%s' % (disease,stat,gene_sets)
            write_results_file(pairs_out_file,results_pair)


    print("initialise LD matrix")
    hdf5_file=config['HDF5_FILE']
    data.init_LD_graph_sparse(hdf5_file,ld='r2')# create LD_mat matrix for all GWIS pairs
        
    # plot heatmap of pairs with genomic coordintes showing r2 as colourbar. 
    # can fix later so only show pairs within 1MB of eachother (less distracting)
    print("plot summary heatmap and histogram")    
    draw_heatmap(data,disease,stat)
    
    # plot histogram
    plot_hist(data,disease,stat)        
    
    # plot bp distance vs ld
    plot_bp_vs_ld(data,disease,stat) # plot currently shows lines instead of dops       


        
    # Find clusters of snps based on bp_distance using bedtools
    print("find clusters")    
    find_clusters(data,snp_bed)    # cluster BED file based on bp distance... 
    data.read_bedfile('clustered.bed') # read in cluster information

    print("create adj_matrix of clusters")    
    data.init_bipartite_matrix()



    print("summarise bipartite subgraphs")
    bipartite_results=summarise_bipartite_subgraphs(data)
    write_results_file('bipartite_subgraph_summary_%s_%s.csv' % (disease,stat), bipartite_results,topline=True)
    hub_results=summarise_hub_subgraphs(data)
    write_results_file('hub_subgraph_summary_%s_%s.csv' % (disease,stat), hub_results,topline=True)

    print("plot top 5 bipartite subgraphs")
    for i in range(5):
        result=bipartite_results[i]
        cluster_edges=result['cluster IDs']
        #view_bipartite_subgraph(data,cluster_edges[0],cluster_edges[1],'%s_%s' % (disease,config['stat']))
                
    print("plot top 5 hub subgraphs")
    for i in range(5):
        result=hub_results[i]
        snp_id=result['hub ID']
        rs_id=data.snp['rs'][snp_id]
        #view_subgraph_rs(data,rs_id,'%s_%s' % (disease,config['stat']),hub_only=True)
        
    print("find gene statistics")
    #find_gene_statistics(data,snp_bed,disease,stat,config)  # calls check_genes function classify_loci - currently causing bugs

    # read in ER+ genes of interest
    bc_file='%sSteroid_Met_Plus_SNPs_140113.csv' % (config['bc_genes_dir'])
    bc_steroid_genes_array=read_breast_cancer_genes(bc_file)
    export_genes_2bed('bc_genes_steroid.bed', bc_steroid_genes_array) # create bed file
    bc_inside=snp_region_intersection(snp_bed,'bc_genes_steroid.bed',left_window=0,right_window=0)
    bc_window_100K=snp_region_intersection(snp_bed,'bc_genes_steroid.bed',left_window=100000,right_window=100000)
    write_results_file('snps_inside_steroid_genes_%s_%s.csv' % (disease,stat),bc_inside,topline=True)
    write_results_file('snps_100K_window_steroid_genes_%s_%s.csv' % (disease,stat),bc_window_100K,topline=True)

    #for rs_id in bc_inside['rs']:
        #view_subgraph_rs(data,rs_id,'%s_%s_%s' % (rs_id,disease,config['stat']))
        #view_bipartite_subgraph_rs(data,rs_id,'%s_%s_%s' % (rs_id,disease,config['stat']))

    #for rs_id in bc_window_100K['rs']:
        #view_subgraph_rs(data,rs_id,'%s_%s_%s' % (rs_id,disease,config['stat']))
        #view_bipartite_subgraph_rs(data,rs_id,'%s_%s_%s' % (rs_id,disease,config['stat']))

    # read in pathway
    #concurrence_results=compute_concurrence(data,config) # compute concurrence scores

    print("data.colour_connected")
    #data.colour_connected()
               
    print("plot large components")
    # not plotting now because too slow
    #plot_large_components(data, disease,config['stat']) #snp2graph functions, filename needed for calling ld
    #plot_large_components_LD(data, disease,stat,hdf5file=hdf5_file,PLOT_R2=True)
    #plot_large_components_gene_name(data, disease,stat)
    #plot_large_components_bp_distance(data, disease,stat)
    
    #print("plot_clustered subgraphs")        
    #plot_clustered_subgraph(data, 2, '%s_%s' % (disease,stat)) 

    # plot all bipartite subgraphs with more than 3 edges
    print('plot bipartite subgraphs')
    #plot_large_bipartite_subgraph(data,'%s_%s' % (disease,stat),3) # min_edges=3, # function needs to be debugged

    print('plot hub subgraphs')
    # not written




   
    #export to sif and attribute files for cytoscape
    data.export2sif('%s_%s.sif' % (disease,stat))
    #data.exportNodeAttributes('%s_%s.NA' % (disease,stat)) # fix to use data.gene_list
    # cleanup
    os.remove('clustered.bed')
    os.remove('%s.bed' % out_file)
    del data

    os.chdir("..") # up one level from stat
    os.chdir("..") # change out of disease 


def _create_short_bed(snp_bed_all_cols):
    """if more than 4 columns, retain only four columns"""
    infile=open('%s' % snp_bed_all_cols,'r')        
    reader = csv.reader(infile, delimiter='\t')
    if len(reader.next()) > 4: 
        snp_bed=export_snp_short_to_bed(snp_bed_all_cols,delimiter='\t', topline=False) 
    else:
        snp_bed='%s.bed' % snp_bed_all_cols # otherwise, process this file.

def _export_snp_short_to_bed(snp_bed_file,delimiter='\t', topline=False):  
    """ reads in bed file created by dbSnp, and exports only first 4 columns, 
    to remove potential problems in dbSnp generated bed file"""    
    snp_bed_short = numpy.genfromtxt(open('%s' % snp_bed_file, 'r'), usecols=(0,1,2,3),
    dtype={'names':['chrom','chromStart','chromEnd','rs',],
               'formats':['S16', int,int,'S16']})
    filename_short='%s_short.bed' % snp_bed_file
    outfile = open(filename_short, 'w') # write a short version
    if topline:
        outfile.write(delimiter.join(snp_bed_short.dtype.names) + '\n')
    for line in snp_bed_short:
        outfile.write(delimiter.join(map(str, line)) + '\n')
    
    outfile.close()        
    return filename_short # return filename of shortened file


if __name__ == '__main__':
    import os, sys
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print 'Usage: python %s <disease> [config]' % sys.argv[0]
        exit(1)
    disease_name = sys.argv[1]
    print disease_name
    if len(sys.argv) == 3:
        config = configure_from_file(sys.argv[2])
    else:
        config = configure_from_file('config_default.py')
    #if os.path.isfile('%s.bim' % loc):
    process(disease_name,config)
