
dataset = 'cruk_clean'
num_pairs = 1000

# PLINK bed, bim, fam files
#genotype_dir = '/Volumes/STORAGE/GWAS/gwis-pp/cruk_clean/A_raw'
genotype_dir = '/Users/cheng.ong/Data/GWAS'

# GWIS results
#gwis_dir = '/Volumes/STORAGE/GWAS/gwis-pp/cruk_clean/F_2ct'
gwis_dir = '/Users/cheng.ong/Data/GWAS'

# JSON file to be written here
rede_dir = '/Users/cheng.ong/Data/Rede'

