
# Bellow produces the JSON file from plink results
# ================================================


# 6 - Run the plink2graph.py that is inside the chillo/tmp-scripts with the small implementation to create the json file from openSNP

# Lactose intolerance
python plink2graph.py --inputtxt lactose_int-plink-epi-0001.epi.txt  --h5name lactose_int --outjson lactose_int-plink-epi-0001.epi.json
# Asthma
python plink2graph.py --inputtxt asthma-plink-epi-0001.epi.txt  --h5name asthma --outjson asthma-plink-epi-0001.epi.json
# Dyslexia
python plink2graph.py --inputtxt dyslexia-plink-epi.epi.txt  --h5name dyslexia --outjson dyslexia-plink-epi.epi.json


