
#from networkx.readwrite import json_graph
from chillo.snp_graph import SnpGraph
from chillo.io_gwis import read_gss_file
from numpy import genfromtxt
from optparse import OptionParser


def process_plink(infilename1,infilename2,outfilename):
    """
    Read the data contained in infilename1 (.txt) and infilename2 (.h5), which should be the outputs
    of the programs plink2hdf5.py and plinkcc2txt.py.
    The outfilename will be the name of json file.
    Construct the SnpGraph, and find connected subgraphs and communities.
    Export the plink result to a JSON file.
    """
    #CHR1         SNP1 CHR2         SNP2       OR_INT         STAT            P position1 position2
  
    filters = ['OR_INT', 'STAT','P']
    
    datafile = genfromtxt(infilename1,
                     dtype={'names':['CHR1', 'SNP1', 'CHR2','SNP2','OR_INT', 'STAT','P', 'position1', 'position2','prb_1','prb_2'],
                   'formats':['S16','S16','S16','S16',float,float,float,int,int,int,int]})



    data = SnpGraph("expt_name")


    data.init_from_sarray_plink(datafile, filters)

    data.set_adj_mat('OR_INT')
    print('Finding connected subgraphs')
    data.colour_connected()
    data.sort_colours()
#    print('Finding communities')     #comment here to not use find communitie
#    data.find_community()            #comment here to not use find communitie
    #I replace this method data.count_edges for data.count_edges_subgraphs() and data.count_edges_communities()
    data.count_edges_subgraphs()
#    data.count_edges_communities()   #comment here to not use find communitie


    json_file = outfilename 
    print('Writing to %s' % json_file)
    data.export_json(json_file,infilename2)


if __name__ == '__main__':
#     process('../test/snp_sig_sel2w.txt')  
#     process('/home/cristovao/epistasis/sel2w4Cris2.txt')


#     process('../test/snp_sig_sel2w.txt')  
#     process('/home/cristovao/epistasis/sel2w4Cris2.txt')

    # build option parser:
    class MyParser(OptionParser):
        def format_epilog(self, formatter):
            return self.epilog
    
    usage = "usage: python %prog [options] filename\n"    

    description = """ 
write some explanation here
"""

    epilog = """ """

    parser = MyParser(usage, description=description,epilog=epilog)
    parser.add_option("--inputtxt",  dest="inputtxt", action="store",
                      help='file .txt - with snp pairs')
    parser.add_option("--h5name", dest="h5name",   action="store",
                      help='file .h5')
    parser.add_option("--outjson", dest="outjson",   action="store",
                      help='output json file')
 

    (options, args) = parser.parse_args()   
    inputtxt = options.inputtxt
    h5name = options.h5name
    outjson = options.outjson


    process_plink(inputtxt,h5name,outjson)

#    process_plink('/home/cristovao/Desktop/AUS_project/opengwas/public_datas/Strabismus/strabismus-plink-epi-01.epi.txt',
#              '/home/cristovao/Desktop/AUS_project/opengwas/public_datas/Strabismus/strabismus',
#               'strabismus-plink-epi-01.json')


    
