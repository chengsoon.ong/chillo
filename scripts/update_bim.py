"""Update the Chromosome and Basepair position information
in the BIM file using data from the chip.
Data is downloaded from the UCSC genome browser.

TODO:
- Cross check the alleles in the two files
"""

import os
import numpy

def load_chip_data(filename, id_name='affy'):
    """Read the chip data and return a numpy structured array
    with the first column from the column id_name,
    and other two columns chromosome, base_pair.
    """
    if id_name == 'affy':
        id_col = 4
    elif id_name == 'rs':
        id_col = 8
    else:
        print("id_name must be 'affy' or 'rs'")
    chrom_col = 1
    bp_col = 2
    chip_data = numpy.genfromtxt(filename, usecols=(id_col, chrom_col, bp_col),
    dtype={'names': [id_name, 'chrom', 'bp_position'],
           'formats': ['S16', 'S4', int]})
    return chip_data

def update(filename, chip_data):
    """Make a copy of filename to filename.orig,
    then write a new file with the chromosome and basepair position updated.
    """
    os.rename(filename, filename+'.orig')
    orig_file = open(filename+'.orig', 'r')
    new_file = open(filename, 'w')
    for line in orig_file:
        convert the line
        
