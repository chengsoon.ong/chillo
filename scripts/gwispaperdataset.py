from numpy import genfromtxt, hstack, vstack, zeros, ones, empty
import sn
import sys,string
import numpy as np
import math
import csv
import os.path
from collections import namedtuple
import os
import vcf
import fnmatch
from optparse import OptionParser
import time
from chillo.snp_graph import SnpGraph
from chillo.io_gwis import read_gss_file




def get_data(filename):
    """
    This function read and return the information inside the file that has the fellow columns:

    'rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS'
    """
    data = genfromtxt(filename,skip_header=1,
                     dtype={'names':['rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS'],
                          'formats':['S16',  'S16',   'S16',   int ,   'S16',   int,      float,       float,       float]})
    return data



def add_prb(infile):
    """
    This function return a numpy array with more two columns from the information in a file that has the SNPs pairs 
    in regarding to GWIS paper dataset.
    The numpy array has 11 columns and the last two columns are ID (prb1 and prb2) for each snp.

    columns:
    'rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS', 'prb1' ,'prb2'
    """

    print "Reading ..."
    data_input = get_data(infile)

    rs =    np.unique(np.append( data_input['rs_1'],data_input['rs_2'] ))

    shape_data=np.shape(data_input)

    full_data = zeros((shape_data[0],), dtype={'names':['rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS', 'prb1' ,'prb2'], 'formats':['S16',  'S16',   'S16',   int ,   'S16',   int,      float,       float,     float,    int,    int]})

    #ofile = open(outfile,'w')    # open file for writing  'rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS'
    print "Writing ..."
    for i in range(shape_data[0]): 
     
        full_data[i]['rs_1'] = data_input[i]['rs_1']
        full_data[i]['rs_2'] =data_input[i]['rs_2']
        full_data[i]['chr_1'] =data_input[i]['chr_1']
        full_data[i]['bp_1'] =data_input[i]['bp_1']
        full_data[i]['chr_2'] =data_input[i]['chr_2']
        full_data[i]['bp_2'] =data_input[i]['bp_2']
        full_data[i]['fltChi2_1'] =data_input[i]['fltChi2_1']  
        full_data[i]['fltChi2_2'] =data_input[i]['fltChi2_2']
        full_data[i]['fltGSS'] =data_input[i]['fltGSS']
        full_data[i]['prb1']=np.flatnonzero(rs==data_input[i]['rs_1'])[0]
        full_data[i]['prb2']=np.flatnonzero(rs==data_input[i]['rs_2'])[0]
    
    return full_data




def process_data(infilename1,outfilename):
    """
    Read the data contained in infilename1 (.txt) and infilename2 (.h5), which should be the outputs
    of the programs plink2hdf5.py and plinkcc2txt.py.
    outfilename will be the name of .json.
    Construct the SnpGraph, and find connected subgraphs and communities.
    Export the result to a JSON file.
    """
    # 'rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS', 'prb1' ,'prb2'
  
    filters = ['fltChi2_1', 'fltChi2_2', 'fltGSS']
    
    #datafile = genfromtxt(infilename1,
    #                 dtype={'names':['CHR1', 'SNP1', 'CHR2','SNP2','OR_INT', 'STAT','P', 'position1', 'position2','prb_1','prb_2'],
    #               'formats':['S16','S16','S16','S16',float,float,float,int,int,int,int]})

    datafile = add_prb(infilename1)

    data = SnpGraph("expt_name")


    data.init_from_sarray_gwis_data(datafile, filters)

    data.set_adj_mat('fltChi2_1')
    print('Finding connected subgraphs')
    data.colour_connected()
    data.sort_colours()
#    print('Finding communities')     #comment here to not use find communitie
#    data.find_community()            #comment here to not use find communitie
    #I replace this method data.count_edges for data.count_edges_subgraphs() and data.count_edges_communities()
    data.count_edges_subgraphs()
#    data.count_edges_communities()   #comment here to not use find communitie


    json_file = outfilename 
    print('Writing to %s' % json_file)
    data.export_json(json_file,False)




if __name__ == '__main__':

    # build option parser:
    class MyParser(OptionParser):
        def format_epilog(self, formatter):
            return self.epilog
    
    usage = "usage: python %prog [options] filename\n"    

    description = """ 
This script write a output file (JSON file) from information txt file with one disease getting from the GWIS paper dataset.
The output file has 11 columns and the last two columns are ID (prb1 and prb2) for each snp.\n
The 11 columns:\t
'rs_1', 'rs_2', 'chr_1', 'bp_1', 'chr_2', 'bp_2', 'fltChi2_1', 'fltChi2_2', 'fltGSS', 'prb1' ,'prb2'
"""

    epilog = """ """

    parser = MyParser(usage, description=description,epilog=epilog)
    parser.add_option("-i",  dest="i", action="store",
                      help='txt file with one disease getting from the GWIS paper dataset')
    parser.add_option("-o", dest="o",   action="store",
                      help='output JSON file')
 

    (options, args) = parser.parse_args()   
    i = options.i
    o = options.o
    
    process_data(i,o)









