
import os
base_dir = os.getcwd()
del os

expt_name = 'opensnp'
result_dir = base_dir + '/results/'

data_dir = '/Users/cheng.ong/Data/openSNP/'
datasets = ['eye_color_case_brown', 'handedness']  # eye_color_case_blue has too many missing genotypes
uni_test = ['assoc','fisher']
hypo_test = ['Chi2','SS','GSS']

num_pairs = 500
