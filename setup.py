#!/usr/bin/env python
from distutils.core import setup, Extension

setup(name = 'Chillo',
      author = 'Cheng Soon Ong, Ilana Lichtenstein, Cristovao Freitas Iglesias Junior',
      author_email = 'chengsoon.ong@unimelb.edu.au',
      version = '0.1',
      description = 'A collection of tools to analyse weigted vertex labeled graphs',
      long_description = open('README').read(),
      url = 'http://www.ong-home.my',
      packages = ['chillo'],
      license = 'GNU GPL version 3 or later',
    )

