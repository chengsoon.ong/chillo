"""A toy example to demonstrate link clustering"""

from numpy import array
import matplotlib.pyplot as plt
import subprocess
from chillo.graph import GWISGraph
from chillo.networkx_tools import view_graph_spring
from chillo.snp_graph import SnpGraph
from chillo.io_gwis import read_gss_file


def demo_toy_data():
    adj_mat1 = array([[0, 1, 1, 1,    0,   0,   0,   0,   0,   0,   0    ],
                      [1, 0, 1, 1,    0,   0,   0,   0,   0,   0,   0    ],
                      [1, 1, 0, 1,    0,   0,   0,   0,   0,   0,   0    ],
		              [1, 1, 1, 0,    0.1, 0.1, 0.2, 0,   0,   0,   0    ],
        		      [0, 0, 0, 0.1,  0,   0.1, 0,   0,   0,   0,   0    ],
		              [0, 0, 0, 0.1,  0.1, 0,   0,   0,   0,   0,   0    ],
        		      [0, 0, 0, 0.2,  0,   0,   0,   0.5, 0.5, 0,   0    ],
        		      [0, 0, 0, 0,    0,   0,   0.5, 0,   0.5, 0,   0    ],
                      [0, 0, 0, 0,    0,   0,   0.5, 0.5, 0,   0,   0    ],
                      [0, 0, 0, 0,    0,   0,   0  , 0 ,  0,   0,   0.9  ],
                      [0, 0, 0, 0,    0,   0,   0  , 0 ,  0,   0.9, 0  ]])


    adj_mat2 = array([[0, 1, 1, 1, 0, 0, 0, 0, 0 ],
                      [1, 0, 1, 1, 0, 0, 0, 0, 0 ],
                      [1, 1, 0, 1, 0, 0, 0, 0, 0 ],
          		      [1, 1, 1, 0, 0.1, 0.1, 0.2, 0, 0 ],
         		      [0, 0, 0, 0.1, 0, 0.1, 0, 0, 0 ],
        		      [0, 0, 0, 0.1, 0.1, 0, 0, 0, 0 ],
        		      [0, 0, 0, 0.2, 0, 0, 0, 0.5, 0.5 ],
        		      [0, 0, 0, 0, 0, 0, 0.5, 0, 0.5 ],
                      [0, 0, 0, 0, 0, 0, 0.5, 0.5, 0 ]])

    v_name1 = ['a','B','c','D','e','F','g','H','i','J','l']   
    v_name2 = ['a','B','c','D','e','F','g','H','i']

    graph = GWISGraph()
    graph.vertex_names = array(v_name2)
    graph.init_graph(adj_mat2)
    graph.colour_connected()
    print('Number of colours = %d' % graph.num_connected_subgraph)
    graph.find_community()
    
    print('Finished clustering')
    print(graph.vertex_names)
    print(graph.adj_mat.todense())
    print "\n"
    print(graph.community_id.todense())

    print "subgraph_id",(graph.subgraph_id)

    #print (graph.extract_subgraph(1).adj_mat.todense())
    #print (graph.extract_subgraph(2).adj_mat)


    #graph.init_networkx()
    #plt.figure()
    #view_graph_spring(graph.graph)
    #plt.show()


def test_gss2graph(filename, diseases, stat_tests):
    """From a table containing pairs and scores from various filters,
    generate a json file.
    The table is in the format output by Matlab GSS.
    The json file is in the format suitable for visualization with probegraph D3.js
    """
    raw_data = read_gss_file(filename)
    for disease in diseases:
        data = SnpGraph(disease+" "+filename)
        idx=raw_data['study']==disease
        disease_data=raw_data[:][idx]
        data.init_from_sarray(disease_data,stat_tests)
        
        data.set_adj_mat('fltGSS')

        print('Finding connected subgraphs')
        data.colour_connected()
        data.sort_colours()
        data.find_community()
        data.count_edges()
        json_file = '%s_%s.json' % (disease, filename)
        print('Writing to %s' % json_file)
        data.export_json(json_file) # Method in SnpGraph, export for D3 visualization

    subprocess.check_call('rm wombat_tiny_gss.txt.json roo_tiny_gss.txt.json', shell=True)

def test_gwis2graph(filename):
    """From a list of pairs, generate a json file.
    The list of pairs are in the format of GWIS.
    The json file is in the format suitable for visualization with probegraph D3.js"""
    out_file = TODO
    data = SnpGraph(filename)

    data.read_gwis_file(filename, TODO )

    data.colour_connected()
    data.sort_colours()
    data.find_community()
    # TO DO
    data.count_edges() # Method in SnpGraph, count the edges in each subgraph and community
    data.export_json(out_file) # Method in SnpGraph, export for D3 visualization


if __name__ == '__main__':
    #demo_toy_data()

    # TODO: Cristovao
    #test_gwis2graph('small_list.txt')
    #test_gwis2graph('les_miserables.txt')

    test_gss2graph('tiny_gss.txt', ['roo', 'wombat'],['fltGSS', 'fltDSS', 'fltSS', 'fltChi2'])
    
