

def test_chillo_pickle():
    """Test load and save"""
    from chillo.io_pickle import load, save
    from chillo.snp_graph import SnpGraph
    import subprocess

    data = SnpGraph('test_example')
    data.read_pairs_csv('test/tiny_gwis.txt')
    save('tiny_list_graph.pkz', data)

    loaded = load('tiny_list_graph.pkz')

    print('Original data')
    print(data.adj_mat.toarray())
    
    print('Reloaded data')
    print(loaded.adj_mat.toarray())
    
    subprocess.check_call('rm tiny_list_graph.pkz', shell=True)

def _read_data(data):
    """Operations that should be the same for all interfaces"""
    print('%d probes, %d individuals' % (data.num_probes, data.num_individuals))
    print('%d cases, %d controls' % (len(data.get_idx_case()), len(data.get_idx_control())))
    print('First 20 individuals from SNP 0 and 42')
    print(data.get_probe(0))
    print(data.get_genotype(0)[:20])
    print(data.get_probe(42))
    print(data.get_genotype(42)[:20])
    
def test_plink(data_file):
    """Test load"""
    from chillo.io_plink import GenotypeData
    data = GenotypeData(data_file, verbose=True)
    data.open_file()
    _read_data(data)
    
def test_hdf5(data_file):
    """Test load"""
    from chillo.io_hdf5 import GenotypeData
    data = GenotypeData(data_file, verbose=True)
    data.open_file()
    _read_data(data)
    data.close_file()
    
def test_sql(data_file):
    """Test load"""
    from chillo.io_sql import GenotypeData
    data = GenotypeData(data_file, verbose=True)
    data.open_file()
    data.init_phenotypes()
    _read_data(data)
    data.close_file()
    
if __name__ == '__main__':
    #test_chillo_pickle()


    data_file = '/Users/cheng.ong/Data/GWAS/celUK1b'
    test_sql(data_file)
    test_hdf5(data_file)
    test_plink(data_file)
