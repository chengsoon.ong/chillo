
import os
base_dir = os.getcwd()
del os

expt_name = 'texpt'
result_dir = base_dir + '/results/'

data_dir = base_dir + '/test/'
datasets = ['plink_test']
uni_test = ['assoc','fisher']
hypo_test = ['Chi2','SS','GSS']

num_pairs = 500
