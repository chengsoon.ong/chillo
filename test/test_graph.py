

def test_toy_data():
    """Test creation of GWISGraph and finding connected components"""
    from scipy.sparse.lil import lil_matrix
    from numpy import array
    from chillo.graph import GWISGraph
    
    adj_mat = lil_matrix([[0,1.5,0,0],
                          [1.5,0,0,0],
                          [0,0,0,3.1],
                          [0,0,3.1,0]])
    graph = GWISGraph()
    graph.vertex_names = array(['a','B','c','D'])
    graph.edge_names = adj_mat
    graph.init_graph(adj_mat)
    graph.graph_statistics()
    graph.colour_connected()
    print('Number of colours = %d' % graph.num_connected_subgraph)
    (colours, counts) = graph.find_large_components(1)
    print('Counts for largest connected subgraphs')
    print(colours)
    print(counts)
    return graph


def test_plot_graph():
    """Test visualization using NetworkX and GML export"""
    import matplotlib.pyplot as plt
    from chillo.networkx_tools import view_graph_spring
    import networkx as nx
    import subprocess

    graph = test_toy_data()
    graph.init_networkx()
    plt.figure()
    view_graph_spring(graph.graph)
    nx.write_gml(graph.graph, 'graph.gml')

    (colours, counts) = graph.find_large_components(1)
    for colour in colours:
        subgraph = graph.extract_subgraph(colour)
        subgraph.init_networkx()
        plt.figure()
        view_graph_spring(subgraph.graph)
    subprocess.check_call('rm graph.gml', shell=True)
